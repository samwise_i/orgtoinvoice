#include "ledger.hpp"


#include <fstream>

#include "error-stuff.hpp"
#include "utils.hpp"

using org::err::putError;
using std::ofstream;
using std::string;

/*
  Write out a Ledger entry for bookeeping
*/
bool WriteLedgerEntry( const std::filesystem::path& baseFileName, const string& clientname, const string& invoiceNum,
                       const string& amount ) {
   string curDate  = org::utils::GetCurDate( "%Y/%m/%d" );
   string projDate = org::utils::GetCurDate( "%Y/%m/XX" );

   std::filesystem::path outfilename{ baseFileName };
   outfilename += ".leg";

   ofstream outfileTex( outfilename );
   if ( !outfileTex.is_open() ) {
      putError( "Error, failed to open output .leg file\n" );
      return false;
   }

   outfileTex << curDate << '=' << projDate << " " << clientname << " Invoice# " << invoiceNum
              << " ; Estimated data of payment\n";
   outfileTex << "    Assets:Accounts Receivable             $" << amount << '\n';
   outfileTex << "    Income:" << clientname << "\n\n";

   outfileTex << ';' << projDate << " **UNPAID** " << clientname << " Invoice# " << invoiceNum
              << " ; Date of actual payment\n";
   outfileTex << ";    Assets:Checking                        $0.00\n";
   outfileTex << ";    Assets:Accounts Receivable" << '\n';

   return true;
}
