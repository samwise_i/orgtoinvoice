#include "database.hpp"

#include <iostream>  // for operator<<, basic_ostream, basic_ostream:...
#include <optional>  // for optional
#include <utility>   // for pair

#include "clock.hpp"        // for Clock_t, CombineClocks
#include "error-stuff.hpp"  // for putError
#include "logging.hpp"      // for LOG

using std::cout;
using std::map;
using std::string;
using std::vector;

using org::err::putError;

namespace org::db {

bool TimeLog::UpdateEntry( LogEntry_t& li, eCommentType ct, const string& comment, const Clock_t& cl ) {
   switch ( ct ) {
      case eCommentType::eNormalWork:
         li.minutesWork += cl.minutes;
         li.commentWork += comment;
         LOG( eLogLevel::Info, "eNormal work update\n" );
         break;
      case eCommentType::eMeeting:
         li.minutesMeetings += cl.minutes;
         li.commentMeetings += comment;
         LOG( eLogLevel::Info, "eMeeting update\n" );
         break;
      case eCommentType::eTravel:
         li.minutesTravel += cl.minutes;
         li.commentTravel += comment;
         LOG( eLogLevel::Info, "eTravel update\n" );
         break;
      case eCommentType::eInvalidComment:
      default:
         /* We should never get here */
         putError( "!!!!Error in Database::updateEntry\n" );
         return false;
   }
   return true;
}

bool TimeLog::UpdateTimeLog( eCommentType ct, const string& comment, const Clock_t& cl ) {
   auto dbi = timeData.find( cl.date );
   if ( dbi != timeData.end() ) {
      /* We found an existing item */
      UpdateEntry( dbi->second, ct, comment, cl );
   } else {
      /* Add new item */
      LogEntry_t le{};
      UpdateEntry( le, ct, comment, cl );
      auto [it, res] = timeData.try_emplace( cl.date, le );
      if ( !res ) {
         putError( "Failed to insert item in database" );
         return false;
      }
   }
   return true;
}

bool TimeLog::Update( eCommentType workType, const string& comment, const vector<Clock_t>& clockList ) {
   switch ( clockList.size() ) {
      case 0:
         putError( "Fatal error, comment before clock." );
         return false;

      case 1: /* Only one clock found before comment so just add the clock and comment to db */
         UpdateTimeLog( workType, comment, clockList[0] );
         break;

      default: /* More than one clock entry preceeds a comment so we must combine clocks */

         if ( auto cl = CombineClocks( clockList ) ) {
            UpdateTimeLog( workType, comment, *cl );
         } else {
            // Error handled by updatetimelog
            LOG( eLogLevel::Error, "UpdateDataBase: Timelog not updated \n" );
         }
         break;
   }
   return true;
}

void TimeLog::Dump() {
   int totalMins{ 0 };

   cout << "Size : " << timeData.size() << '\n';

   for ( auto& iter : timeData ) {
      cout << "Date : " << iter.first << '\n';

      if ( iter.second.minutesWork ) {
         cout << "  " << iter.second.minutesWork << " - " << iter.second.commentWork << '\n';
         totalMins += iter.second.minutesWork;
      }
   }

   cout << "\nTotal Minutes :  " << totalMins << '\n';
   cout << "\nTotal Hours   :  " << static_cast<float>( totalMins ) / 60.0f << '\n';
}

}  // end namespace org::db
