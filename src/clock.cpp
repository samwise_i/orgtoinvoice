#include "clock.hpp"

#include <assert.h>
#include <stdio.h>

#include "error-stuff.hpp"
#include "utils.hpp"

using std::optional;
using std::string;
using std::vector;

using org::err::printError;
using org::err::putError;

bool FilterDayMonYear( const Clock_t &ct, int bday, int eday, int m, int y ) {
   if ( ct.year == y ) {
      if ( ct.month == m ) {
         if ( ( 0 == bday ) && ( 0 == eday ) )
            return true;

         if ( ( ct.day >= bday ) && ( ct.day <= eday ) )
            return true;
      }
   }
   return false;
}

static optional<DateStruct_t> makeDateStruct( const string &date ) {
   DateStruct_t ds{};

   sscanf( date.c_str(), "%4d-%2d-%2d", &ds.year, &ds.month, &ds.day );
   ds.date = date;

   /* Sanity checks */
   if ( !org::utils::IsInBounds( ds.year, 2000, 2050 ) )
      return {};

   if ( !org::utils::IsInBounds( ds.month, 1, 12 ) )
      return {};

   if ( !org::utils::IsInBounds( ds.day, 1, 31 ) )
      return {};

   return { ds };
}

static bool isDayOfWeekValid( const string &input ) {
   const char *validDays[] = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

   for ( auto &ds : validDays ) {
      if ( ds == input )
         return true;
   }
   return false;
}

static int convertToMinutes( const string &timeValue ) {
   size_t dividerLoc = timeValue.find_first_of( ':' );
   string hours      = timeValue.substr( 0, dividerLoc );
   string minutesStr = timeValue.substr( dividerLoc + 1 );

   /* Keep totaling our minutes until we reach a comment which will reset this. */
   int currentMinutes = ( stoi( hours ) * 60 ) + stoi( minutesStr );
   return currentMinutes;
}

/*
  Combine two or more Clock_t structs with some checks and sum the minutes.
  This is necessary since we can only store one clock per comment in the database
*/
optional<Clock_t> CombineClocks( const vector<Clock_t> &cl ) {
   Clock_t rclock( cl[0] );

   for ( size_t i = 1; i < cl.size(); i++ ) {
      if ( cl[0].date != cl[i].date ) {
         // DBG printf("cl[0] %  cl[i] %s\n",  cl[0].date, cl[i].date );
         putError( "Combine Clocks Failed, one comment per clock day required\n" );
         return {};
      }
      rclock.minutes += cl[i].minutes;
   }
   return { rclock };
}

static optional<DateStruct_t> extractDate( const string &dateStr ) {
    DateStruct_t rDate{};

   auto extractedDate = org::utils::GetData( dateStr );
   if ( !extractedDate ) {
      putError( "Invalid start date format\n" );
      return {};
   }

   auto dss = makeDateStruct( *extractedDate );
   if ( !dss ) {
      putError( "Invalid data range\n" );
      return {};
   }

   /* Note that we only do this for the start date and not the end
    * Since we assume that no clock overflow into the next day */

   rDate.date  = dss->date;
   rDate.day   = dss->day;
   rDate.month = dss->month;
   rDate.year  = dss->year;

   return { rDate };
}

/*
  This grabs the start and end dates making sure that they match.
  If they match and are in a valid format then save the start date
  in our clock structure
*/
static optional<DateStruct_t> checkDates( const vector<string> &tokens ) {
   auto dateStart = extractDate( tokens[eStartDate] );
   if ( !dateStart )
      return {};

   auto dateEnd = extractDate( tokens[eEndDate] );
   if ( !dateEnd )
      return {};

   /* Need to make sure that begin date is the same as end date since we do
    * not allow wrapping for date entries */
   if ( *dateEnd != dateStart ) {
      printError( "Date Mismatch start[%s] - end[%s]", tokens[eStartDate].c_str(), tokens[eStartDate].c_str() );
      return {};
   }

   return { dateStart };
}

/*
   This function takes a vector<string> which contains a tokenized OrgMode clock line:

   0        1             2     3        4             5     6        7    8
   CLOCK: ^ [2010-10-18 ^ Mon ^ 10:23] ^ [2010-10-18 ^ Mon ^ 14:14] ^ => ^ 3:51 ^

   This tokenized form is checked for sanity, cleaned up, and placed in a structure
 */
optional<Clock_t> ProcessClock( const vector<string> &tokens ) {
    Clock_t clock{};

   auto startDate = checkDates( tokens );
   if ( !startDate ) {
      putError( "Check date Failed\n" );
      return {};
   }

   if ( false == isDayOfWeekValid( tokens[eDayOfWeek] ) ) {
      putError( "Failed day of week\n" );
      return {};
   }
   clock.dayOfWeek = tokens[eDayOfWeek];

   string resHours = org::utils::GetHours( tokens[eHours] );
   if ( "" == resHours ) {
      putError( "Failed to caclulate hours\n" );
      return {};
   }

   int minutes = convertToMinutes( resHours );

#if defined(DONT_ALLOW_ZERO_MINUTES)
   if ( 0 == minutes ) {
      putError( "Error processing clock, zero minutes\n" );
      return {};
   }
#endif

   clock.date    = startDate->date;
   clock.day     = startDate->day;
   clock.month   = startDate->month;
   clock.year    = startDate->year;
   clock.minutes = minutes;

   return { clock };
}
