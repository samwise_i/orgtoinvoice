#pragma once

#include <cstdint>
#include <optional>
#include <string>
#include <string_view>  // for string_view
#include <utility>
#include <vector>

namespace org::utils {

enum eLineTypeClk { eLineClock = 0, eLineWork, eLineMeeting, eLineTravel, eLineUnKnown, eNumLineTypesClk };

enum class eLineTypeCFG : int { eLineSection = 0, eLineNameVal, eLineComment, eLineUnknownCFG, eNumLineTypesCFG };

std::string                          EscapifyComment( const std::string &comment );
std::string                          FormatInvoice( uint64_t invoiceNum );
int                                  GetMonth( std::string_view &dateValue );
std::string                          GetCurDate( const char *fmtStr );
std::string                          RemoveLeadingChars( const std::string &str, const char *ignoreChars );
std::string                          RemoveTrailingChar( const std::string &str, char ignoreChar );
eLineTypeClk                         DetermineLineType( const std::string &line );
std::pair<eLineTypeCFG, std::string> DetermineLineCFG( const std::string &line );
std::vector<std::string>             ClockLineLexer( const std::string &line );
std::pair<std::string, std::string>  ParseKeyValue( const std::string &str, char sep );
std::optional<std::string>           GetData( const std::string &input );
std::string                          GetHours( const std::string &input );
std::optional<std::string>           IsComment( const std::string &input );

inline void MakeLowerCase( std::string &str ) {
   for ( auto &c : str ) c = static_cast<char>( tolower( c ) );
}

inline void MakeUpperCase( std::string &str ) {
   for ( auto &c : str ) c = static_cast<char>( toupper( c ) );
}

template <typename T>
bool IsInBounds( const T &value, const T &low, const T &high ) {
   return ( value >= low ) && ( value <= high );
}

}  // end namespace org::utils
