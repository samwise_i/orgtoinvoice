#pragma once

#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

struct Clock_t;

namespace org::db {

/* Value portion of our log structure these are indexed by a date in string form */
struct LogEntry_t {
   int         minutesWork{ 0 };
   int         minutesMeetings{ 0 };
   int         minutesTravel{ 0 };
   std::string dayOfWeek;
   std::string commentWork;
   std::string commentMeetings;
   std::string commentTravel;
};

using TimeData_t = std::map<std::string, LogEntry_t>;

enum class eCommentType { eNormalWork, eMeeting, eTravel, eInvalidComment };

class TimeLog {
   TimeData_t timeData;

   bool UpdateEntry( LogEntry_t& li, eCommentType ct, const std::string& comment, const Clock_t& cl );
   bool UpdateTimeLog( eCommentType ct, const std::string& comment, const Clock_t& cl );

  public:
   const TimeData_t& GetTimeData() const { return timeData; }
   bool              Update( eCommentType workType, const std::string& comment, const std::vector<Clock_t>& clockList );
   void              Dump();
};

}  // end namespace org::db
