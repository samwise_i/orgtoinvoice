/*

  All time entries from the .org file shall have the following format:

  CLOCK: [2017-02-14 Tue 13:16]--[2017-02-14 Tue 15:22] =>  2:06
  ...
  CLOCK: [2017-02-14 Tue 17:14]--[2017-02-14 Tue 18:15] =>  1:01
  ` Worked with Elon on creating a release candidate and running torture tests.

  The CLOCK: lines are auto generated by org-mode using the built-in clock functionality.
  There may be one of more of these clock lines per comment which describes what
  was done for that period(s). Note, comment lines must start with (~,`,or ^) and follow 1 or
  more valid clock lines.

  Work days can be entered in any order since they are sorted when read.

  All comments are required to start with one of the following:
  ~         Normal work
  `         Meeting
  ^         Travel (Not implemented)

  A configuration file (with a .cfg extension) shall be provided, which includes details about the provider and client,
  such as:

  [PROVIDER]
  Company = "Awesome Engineering, Inc."
  Address1 = "10 Where stuff gets done circle"
  City = "TheCity"
  State =  "CA"
  ZIP =  "55555"
  Phone = "415.555.1212"

  [CLIENT]
  Company = "ACME Systems, Inc."
  Address1 = "18 Cool Project Street"
  City = "TheCity"
  State =  "TX"
  ZIP =  "55555"
  Phone = "512.555.1212"
  PO = "12345"
  Rate = "1000.00"

  Typical usage :

  orgtoinv -r 1000.00 -i 4 -m 8 -p 1 -s ~/<path to .org file>/<client>.org -y 2022 -c <ClientProvider>.cfg

*/
#include <getopt.h>

#include <cassert>
#include <cstdio>
#include <filesystem>
#include <memory>
#include <string>

#include "cfg-reader.hpp"
#include "database.hpp"
#include "error-stuff.hpp"
#include "invoice.hpp"
#include "ledger.hpp"
#include "logging.hpp"
#include "money-math.hpp"
#include "process-org.hpp"
#include "utils.hpp"
#include "worklog.hpp"

using std::string;
using std::to_string;
using std::unique_ptr;

using org::err::printError;
using org::err::putError;

/* Begin and End day within a month to describe a pay period */
struct PayPeriod {
   int beg{ 0 };
   int end{ 0 };
};

/* Define some things about various months that makes generating reports easier */
struct MonthInfo {
   string    name;
   int32_t   days{ 0 };
   PayPeriod payPeriod[2];
};

/*
  Used for determining pay periods, change as needed. The idea here allows for
  semi-montly billing. The date range for each period is specfied by the defined
  PayPeriods. For example in the table below the first pay period for the month
  of Jan is from 01 Jan to 15 Jan and the second PayPeriod is from 16 Jan to 31 Jan.

  Format:
  { "Month", daysInMonth, { PayPeriod1{begDay, endDay}, PayPeriod2{begDay, endDay} } }
*/
static const MonthInfo monthInfo[] = {
   /* clang-format off */
   { "",0,{ {0,0},{0,0}} },
   { "Jan", 31, { {1, 15}, {16, 31} } },
   { "Feb", 28, { {1, 14}, {15, 28} } },
   { "Mar", 31, { {1, 15}, {16, 31} } },
   { "Apr", 31, { {1, 15}, {16, 30} } },
   { "May", 31, { {1, 16}, {15, 31} } },
   { "Jun", 31, { {1, 15}, {16, 30} } },
   { "Jul", 31, { {1, 16}, {17, 31} } },
   { "Aug", 31, { {1, 15}, {16, 31} } },    
   { "Sep", 31, { {1, 15}, {16, 30} } },
   { "Oct", 31, { {1, 15}, {16, 31} } },
   { "Nov", 31, { {1, 15}, {16, 30} } },
   { "Dec", 31, { {1, 15}, {16, 31} } }
   /* clang-format on */
};

static void signOnMsg( void ) {
   printf( "%s", org::err::HIGrn );
   puts( R"(   ____            ______      ____                 _         )" );
   puts( R"(  / __ \_________ /_  __/___  /  _/___ _   ______  (_)_______ )" );
   puts( R"( / / / / ___/ __ `// / / __ \ / // __ \ | / / __ \/ / ___/ _ \)" );
   puts( R"(/ /_/ / /  / /_/ // / / /_/ // // / / / |/ / /_/ / / /__/  __/)" );
   puts( R"(\____/_/   \__, //_/  \____/___/_/ /_/|___/\____/_/\___/\___/ )" );
   puts( "          /____/               Ver 0.9.4, \xc2\xa9 Samwise 2015-24\n\n\n" );
   printf( "%s", org::err::ColorOff );
}

static void printUsage() {
   puts( "Options:\n" );
   /* clang-format off */
   printf( "  %-40s %s\n", "  --time     | -t [input file]", ".org input file with time data" );
   printf( "  %-40s %s\n", "  --config   | -c [input file]", ".cfg configuration file, defaults to same filename as time data if not specified" );
   printf( "  %-40s %s\n", "  --invoice  | -i [invoice#]", "Invoice number" );
   printf( "  %-40s %s\n", "  --month    | -m [month]", "Month 1-12" );
   printf( "  %-40s %s\n", "  --year     | -y [year]", "Year, defaults to current year is not specified" );
   printf( "  %-40s %s\n", "  --bday     | -b [day of month]", "Start day pay period, NOT used if pay period specified" );
   printf( "  %-40s %s\n", "  --eday     | -e [day of month]", "End day pay period, NOT used if pay period specified" );
   printf( "  %-40s %s\n", "  --period   | -p [Pay period]", "Pay period either 1 or 2, overrides -b and -e options" );
   printf( "  %-40s %s\n", "  --PO       | -o [PO number]", "Purchase order number" );
   printf( "  %-40s %s\n", "  --rate     | -r [dollars/hour]", "Hourly rate" );
   printf( "  %-40s %s\n", "  --fbase    | -f [base filename]", "Override filename generation. Used when driven by a script" );   
   printf( "  %-40s %s\n", "  --ledger   | -l", "Generate a ledger entry" );
   printf( "  %-40s %s\n", "  --speriods | -s", "List pay periods" );   
   printf( "  %-40s %s\n", "  --help     | -h", "This output" );
   printf( "  %-40s %s\n", "  --verbose  | -v", "Verbose output\n" );

   puts( "You MUST provide :\n" );
   puts( "    --time [input file.org]" );
   puts( "    --config [input file.cfg] NOT necessary if base filename same as .org, e.g. (customer.org, customer.cfg)" );
   puts( "    --invoice [invoice number]" );
   puts( "    --month [1-12]" );
   puts( "    --period [1-2] <OR> ( -bday [1-31] <AND> -eday [1-31] )" );
   /* clang-format on */
   puts( "\n" );
}

static void printPayPeriods() {
   puts( "\nPay periods:\n" );
   for ( int i{ 1 }; i < 13; ++i ) {
      printf( "%s P1[%d thru %d], P2[%d thru %d]\n", monthInfo[i].name.c_str(), monthInfo[i].payPeriod[0].beg,
              monthInfo[i].payPeriod[0].end, monthInfo[i].payPeriod[1].beg, monthInfo[i].payPeriod[1].end );
   }
   puts( "\n" );
}

/*
  Main entry point is a little long due to the number of user options.
*/
int main( int argc, char *argv[] ) {
   std::filesystem::path inputFile;
   std::filesystem::path configFile;
   std::filesystem::path baseFileName;
   string                payPer;
   string                rate;
   string                poNum;
   string                invoiceNumber;
   string                month{ "0" };
   string                begDay{ "0" };
   string                endDay{ "0" };
   string                year = org::utils::GetCurDate( "%Y" );
   bool                  generateLedger{ false };
   bool                  showPayPeriods{ false };
   bool                  verbose{ false };

   int optidx{0};
   int opt{0};

   /* clang-format off */
   constexpr struct option longopt[] = { 
      { "config", 1, NULL, 'c' },
      { "time", 1, NULL, 't' },
      { "month", 1, NULL, 'm' },
      { "invoice", 1, NULL, 'i' },
      { "year", 1, NULL, 'y' },
      { "bday", 1, NULL, 'b' },
      { "eday", 1, NULL, 'e' },
      { "PO", 1, NULL, 'o' },
      { "period", 1, NULL, 'p' },
      { "rate", 1, NULL, 'r' },
      { "filebase", 1, NULL, 'f' },      
      { "ledger", 0, NULL, 'l' },
      { "speriods", 0, NULL, 's' },      
      { "help", 0, NULL, 'h' },
      { "verbose", 0, NULL, 'v' },
      { 0, 0, 0, 0 } };
   /* clang-format on */

   while ( ( opt = getopt_long_only( argc, argv, "c:t:m:i:y:b:e:o:p:r:f:lshv", longopt, &optidx ) ) != -1 ) {
      switch ( static_cast<char>( opt ) ) {
            /* Required : Specify the org time input file */
         case 't':
            inputFile.assign( optarg );
            break;

            /* Optional : Must specify a config(cfg) file if name differs from .org file */
         case 'c':
            configFile.assign( optarg );
            break;

            /* Required : */
         case 'i':
            invoiceNumber.assign( optarg );
            break;

            /* Required : Specify for pay period 1 or 2 OR begin and end days*/
         case 'p':
            payPer.assign( optarg );
            break;

            /* Begin day : Not used if pay period specified */
         case 'b':
            begDay.assign( optarg );
            break;

            /* End day : Not used if pay period specified */
         case 'e':
            endDay.assign( optarg );
            break;

            /* Optional : Rate/hr if specified will override .cfg file */
         case 'r':
            rate.assign( optarg );
            break;

            /* Required : Specify the month to be reported */
         case 'm':
            month.assign( optarg );
            break;

            /* PO number */
         case 'o':
            poNum.assign( optarg );
            break;

            /* Used when scripted */
         case 'f':
            baseFileName.assign( optarg );
            break;

            /* If specified overrides current year */
         case 'y':
            year.assign( optarg );
            break;

         case 'h':
            // TODO: add better help
            signOnMsg();
            printUsage();
            return 0;
            break;

         case 'l':
            generateLedger = true;
            break;

         case 's':
            showPayPeriods = true;
            break;

         case 'v':
            signOnMsg();
            verbose = true;
            break;

         default:
            org::err::printError( "Bad arg: %c\n", opt );
            printUsage();
            return 1;
      }
   }

   /* Figure out .cfg file name since special rules apply */

   /* If a .cfg file exists with the same name as the .org(less ext) then we */
   /* use this file instead of a requiring the user to supply a path for the .cfg */
   /* file. If the user does supply a path for the .cfg then use what the user */
   /* supplied. */
   std::filesystem::path configFileName{ inputFile };
   if ( configFile.empty() ) {
       configFileName.replace_extension( ".cfg" );
   } else {
       configFileName =configFile;
   }
   if( !std::filesystem::exists(configFileName) )
       printError( "Could not find configuration file %s\n", configFileName.c_str() );

   LOGF( eLogLevel::Info, "Opening .cfg file [%s]\n", configFileName.c_str() );

   if ( showPayPeriods == true ) {
      printPayPeriods();
      return 0;
   }


   /* Get provider and client information from user supplied config(cfg) file */
   if ( ProcessConfigFile( configFileName ) ) {
      putError( "Error processing configuration file\n" );
      return -1;
   }

   /* User can enter a rate on the command line or the config file figure out which to use or error if none */
   /* Command line overrides config file value */
   using namespace MMath;
   mm_t ratehr{ 0.0 };
   if ( rate.empty() ) {
      if ( "" == ( rate = GetConfigData( "CLIENT-rate" ) ) ) {
         putError( "!!! Error MUST enter hourly rate\n" );
         return -1;
      } else {
         ratehr = rate;
      }
   } else {
      ratehr = rate;
   }

   /* Convert and check for invariants */
   uint64_t invoice{ 0 };
   int      mnth{ 0 };
   if ( invoiceNumber.empty() ) {
      putError( "!!! Error MUST enter invoice number\n" );
      return -1;
   } else {
      invoice = stoul( invoiceNumber );
   }

   if ( month.empty() ) {
      putError( "!!! Error MUST enter month\n" );
      return -1;
   } else {
      mnth = stoi( month );
      if ( !org::utils::IsInBounds<int>( mnth, 1, 12 ) ) {
         putError( "!!! Invalid Month\n" );
         return -1;
      }
   }

   /* First check to see if pay period was defined, then create work period */
   int bDay = stoi( begDay );
   int eDay = stoi( endDay );
   if ( payPer.empty() ) {
      /* No pay period selected so use beg/end user entered with range checking */
      bDay = stoi( begDay );
      if ( !org::utils::IsInBounds<int>( bDay, 1, monthInfo[mnth].days ) ) {
         putError( "!!! Invalid begin day\n" );
         return -1;
      }

      eDay = stoi( endDay );
      if ( !org::utils::IsInBounds<int>( eDay, 1, monthInfo[mnth].days ) ) {
         putError( "!!! Invalid end day\n" );
         return -1;
      }

   } else {
      int pp = stoi( payPer );
      if ( org::utils::IsInBounds<int>( pp, 1, 2 ) ) {
         bDay = monthInfo[mnth].payPeriod[pp - 1].beg;
         eDay = monthInfo[mnth].payPeriod[pp - 1].end;
      } else {
         putError( "!!! Error Pay Period MUST be between 1 and 2\n" );
         return -1;
      }
   }

   std::filesystem::path fileName{ inputFile.filename() };
   string                payPeriod;
   string                formattedInv = org::utils::FormatInvoice( invoice );
   if ( baseFileName.empty() ) {
      /* Base file name was NOT supplied, so generate one  */
      baseFileName = inputFile.stem();
      baseFileName += '-';

      if ( ( bDay != 0 ) && ( eDay != 0 ) ) {
         /* Use pay period  */
         payPeriod = monthInfo[mnth].name + ' ' + to_string( bDay ) + '-' + to_string( eDay ) + ", " + year;
         baseFileName +=
             formattedInv + '.' + monthInfo[mnth].name + '-' + to_string( bDay ) + '-' + to_string( eDay ) + '-' + year;
      } else {
         putError( "!!! Invalid pay period\n" );
      }
   }

   LOGF( eLogLevel::Info, "Opening .org file [%s]\n", inputFile.c_str() );

   /* Read the .org file and update our database */
   org::db::TimeLog timeLog;
   if ( ProcessOrgFile( inputFile, bDay, eDay, mnth, stoi( year ), &timeLog ) ) {
      printError( "Unable to process .org file\n" );
      return -1;
   }

   // DBG timeLog.dump();

   unique_ptr<WorkLog> pWorkLog{ new WorkLogColor };
   auto                totalHours = pWorkLog->write( baseFileName, timeLog, payPeriod, verbose );
   if ( !totalHours ) {
      printError( "Unable to write log file\n" );
   }

   unique_ptr<Invoice> pInvoice{ new InvoiceColor };
   auto                totalAmount = pInvoice->write( baseFileName, timeLog, invoice, poNum, ratehr );
   if ( !totalAmount ) {
      printError( "Unable to write invoice file\n" );
   }

   printf( "\nOutput written to : %s\n\n", baseFileName.c_str() );

   if ( generateLedger == true ) {
      WriteLedgerEntry( fileName, GetConfigData( "CLIENT-company" ), invoiceNumber, *totalAmount );
   }

   return 0;
}
