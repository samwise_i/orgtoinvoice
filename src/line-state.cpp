
#include "line-state.hpp"

#include <cassert>

/* Org-mode log states */
enum eState {
   sInit      = 0,
   sLook4Clk  = 1,
   sProcClk   = 2,
   sErr       = 3,
   sNumStates = 4,

   /* Action Defined - This allows for the action to determine the next state instead of
      the state table. The is the case of we need to know where we came from. */
   sActDfnd = 0xFF
};

using enum eAction;
struct stateElement_t {
   eState  nextState;
   eAction actionToDo;
};

/* Keep track of our state */
static eState currentState = sLook4Clk;

/* Org-mode valid line sequences,  */

static const stateElement_t stateMatrix[sNumStates][org::utils::eNumLineTypesClk] = {
   /* clang-format off */
   /*  eLineClock                  eLineWork                 eLineMeeting               eLineTravel                eLineUnknown    */
   {   /* 0 - Init */
       { sErr, aError },           { sErr, aError },         { sErr, aError },          { sErr, aError },          { sInit, aNone }
   },
   {   /* 1 - Look 4 clock */
       { sProcClk, aProcClk },     { sLook4Clk, aError },    { sLook4Clk, aError },     { sLook4Clk, aError },     { sLook4Clk, aNone }
   },
   {   /* 2 - Process clock */
       { sProcClk, aProcClk },     { sLook4Clk, aProcWrk },  { sLook4Clk, aProcMeet },  { sLook4Clk, aProcTrav },  { sProcClk, aNone }
   }
   /* clang-format on */
};

/*
  This function takes the newly received org line type and returns the action
  to be performed if any. The state is also updated per the state table.
*/
eAction StateEval( org::utils::eLineTypeClk lineType ) {
   assert( currentState < sNumStates );
   assert( lineType < org::utils::eNumLineTypesClk );

   /* Determine the State-Matrix-Element in dependany of current state and triggered event */
   stateElement_t stateEvaluation = stateMatrix[currentState][lineType];

   /* Check to see if we want our action to dermine next state, or just look it up in the state matrix */
   if ( sActDfnd != stateEvaluation.nextState )
      currentState = stateEvaluation.nextState;

   /* Now figure which action to execute */
   eAction actToDo = stateEvaluation.actionToDo;

   return actToDo;
}
