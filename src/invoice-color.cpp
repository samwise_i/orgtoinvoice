#include <cstdio>
#include <memory>
#include <string>
#include <utility>

#include "LaTeX-helper.hpp"
#include "cfg-reader.hpp"
#include "database.hpp"
#include "error-stuff.hpp"
#include "invoice.hpp"
#include "money-math.hpp"
#include "utils.hpp"

using std::cout;
using std::fixed;
using std::ofstream;
using std::optional;
using std::showpoint;
using std::string;
using std::string_view;

using org::err::printError;

using MMath::mm_t;
using MMath::MoneyMath_cast;

/* clang-format off */
static constexpr string_view setup[] = {
   /* Import our template */
   R"(\documentclass[12pt]{report})",
   R"(\usepackage[T1]{fontenc})",
   R"(\usepackage{lmodern})",
   R"(\usepackage{booktabs})",
   /* Better for color tables */
   R"(\usepackage{tabu})",
   /* Enable color */
   R"(\usepackage{color, colortbl})",
   R"(\usepackage[table]{xcolor})",
   /* Do not indent paragraphs */
   R"(\usepackage[parfill]{parskip})",
   /* Use most of the page */
   R"(\usepackage[hmargin=0.5in,vmargin=0.5in]{geometry})"
   /* Grab the user colors */
   R"(\usepackage{import})",
   R"(\import{./}{invoice-color.tex})"
};
/* clang-format on */

void InvoiceColor::writeProvider( ofstream& outfileTex, uint64_t invoiceNum ) {
   string curDate = org::utils::GetCurDate( "%d %b %Y" );

   // DBG small caps version - outfileTex << R"(\textsc{\LARGE )" << getConfigData( "PROVIDER-company" ).c_str() << "}

   WrapTex wt( GetConfigData( "PROVIDER-company" ), "providercolor" );
   outfileTex << R"({\LARGE )" << *wt << "} ";

   wt.assign( org::utils::FormatInvoice( invoiceNum ), "invoicecolor" );
   outfileTex << R"({\large \hfill Invoice Number : )" << *wt << R"(} \\)" << '\n';

   wt.assign( GetConfigData( "PROVIDER-address1" ), "addresscolor" );
   outfileTex << *wt << R"( \hfill )" << curDate << R"( \\)" << '\n';

   char outStr[256];
   snprintf( outStr, sizeof( outStr ), "%s, %s    %s", GetConfigData( "PROVIDER-city" ).c_str(),
             GetConfigData( "PROVIDER-state" ).c_str(), GetConfigData( "PROVIDER-zip" ).c_str() );

   wt.assign( outStr, "addresscolor" );
   outfileTex << *wt << R"( \\)" << '\n';
}

void InvoiceColor::writeCustomer( ofstream& outfileTex, const string& poNum ) {
   outfileTex << R"(\vspace*{2ex} \\)" << '\n';
   outfileTex << R"({\LARGE Invoice To:})"
              << R"( \\)" << '\n';

   WrapTex wt( GetConfigData( "CLIENT-company" ), "addresscolor" );
   outfileTex << R"(\tab )" << *wt;
   if ( poNum != "" ) {
      wt.assign( poNum, "black" );
      outfileTex << R"( \hfill PO : )" << *wt;
   }
   outfileTex << R"( \\)" << '\n';

   wt.assign( GetConfigData( "CLIENT-address1" ), "addresscolor" );
   outfileTex << R"(\tab )" << *wt << R"( \\)" << '\n';

   char outStr[256];
   snprintf( outStr, sizeof( outStr ), "%s, %s    %s", GetConfigData( "CLIENT-city" ).c_str(),
             GetConfigData( "CLIENT-state" ).c_str(), GetConfigData( "CLIENT-zip" ).c_str() );
   wt.assign( outStr, "addresscolor" );
   outfileTex << R"(\tab )" << *wt << '\n';
}

void InvoiceColor::writeFooter( ofstream& outfileTex ) {
   outfileTex << R"(\vfill)" << '\n';
   outfileTex << R"(\flushleft)" << '\n';

   string terms = GetConfigData( "CLIENT-terms" );
   if ( "" != terms ) {
      string termStr{ "Terms: " };
      termStr += terms;
      WrapTex wt( termStr, "termscolor" );
      outfileTex << *wt << R"( \hfill )";
   }

   WrapTex wt( "Thank you for your business:-)", "footerblue" );
   outfileTex << *wt << R"( \\)" << '\n';
}

optional<const string> InvoiceColor::write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                            uint64_t invoiceNum, const string& poNum, const mm_t& ratePerHour ) {

    std::filesystem::path outfilename{ fileName };
    outfilename += ".tex";

   ofstream outfileTex( outfilename );
   if ( !outfileTex.is_open() ) {
      printError( "Error, failed to open output tex file[%s]\n", outfilename.c_str() );
      return {};
   }

   /* Setup the document */
   for ( auto& config : setup ) {
      outfileTex << config << '\n';
   }

   /* Start the document */
   outfileTex << R"(\begin{document})" << '\n';
   outfileTex << R"(\fontfamily{lmss}\selectfont)" << '\n';
   outfileTex << R"(\def \tab {\hspace*{3ex}} % Define \tab to create some horizontal white space)" << '\n';

   outfileTex << R"({\color{headercolor}\hrule height4mm})" << '\n';
   outfileTex << R"(\vspace*{2ex})" << '\n';

   writeProvider( outfileTex, invoiceNum );

   writeCustomer( outfileTex, poNum );

   /* Set up for writing the time data to a LaTeX tabu table */
   outfileTex << R"(\small)" << '\n';
   outfileTex << R"(\flushright)" << '\n';
   outfileTex << R"(\vspace*{3ex})" << '\n';

   outfileTex << R"(\taburulecolor{tablearraycolor})" << '\n';
   outfileTex << R"(\begin{tabu}{l@{\hspace{ 14em }}r@{\hspace{2em}}rr})" << '\n';

   outfileTex << R"(\rowfont{\color{tableheadercolor}})" << '\n';
   outfileTex << R"({\large Date} & {\large Hours} & {\large Rate} & {\large Amount} \\)" << '\n';

   outfileTex << R"(\toprule[1.5pt])" << '\n';

   mm_t    hours{ 0.0f };
   mm_t    totalHours{ 0.0f };
   mm_t    subtotal{ 0.0f };
   mm_t    totalAmount{ 0.0f };
   WrapTex wt( R"(\tiny /hr)", "black" );
   string  rateStr = "\\$" + ratePerHour.toString() + *wt;
   string  totalString{ "$" };

   char outStr[256];
   for ( const auto& [date, info] : db.GetTimeData() ) {
      /* Change the color of table entries */
      outfileTex << R"(\rowfont{\color{tablecolor1}})" << '\n';

      hours    = MoneyMath_cast<2>( info.minutesWork ) / MoneyMath_cast<2>( 60.00 );
      subtotal = hours * ratePerHour;
      totalHours += hours;
      totalAmount += subtotal;

      totalString += subtotal.toString();
      snprintf( outStr, sizeof( outStr ), R"(%s & %-6s & %-8s & \%-10s \\)", date.c_str(), hours.toCString(),
                rateStr.c_str(), totalString.c_str() );
      outfileTex << outStr << '\n';

      /* Only print one rate per invoice */
      rateStr     = "";
      totalString = "$";
   }

   outfileTex << R"(\midrule)" << '\n';
   outfileTex << R"(\noalign{\vskip 2mm})" << '\n';

   snprintf( outStr, sizeof( outStr ), "~~Total  %10s    ~%10s\n", totalHours.toCString(), totalAmount.toCString() );
   totalString = "$";
   totalString += totalAmount.toString();
   snprintf( outStr, sizeof( outStr ), R"(\multicolumn{3}{r}{\large Total} & {\large \%s} \\)", totalString.c_str() );
   outfileTex << outStr << '\n';

   outfileTex << R"(\noalign{\vskip 2mm})" << '\n';
   outfileTex << R"(\bottomrule[1.5pt])" << '\n';

   outfileTex << R"(\end{tabu})" << '\n';

   writeFooter( outfileTex );

   outfileTex << R"(\end{document})" << '\n';

   return { totalString };
}
