#pragma once

namespace org::err {

inline const char HIGrn[]    = "\033[0;92m";
inline const char HIRed[]    = "\033[0;91m";
inline const char ColorOff[] = "\033[0m";

/* Print an error to the console in RED */
void putError( const char *errorString );

/* Print a formatted error to the console in RED */
void printError( const char *format, ... );

} // end namespace org::err
