#pragma once

#include <filesystem>
#include <fstream>
#include <optional>
#include <string>
#include <utility>

#include "database.hpp"
#include "error-stuff.hpp"
#include "money-math.hpp"

struct Invoice {
   virtual std::optional<const std::string> write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                                   const uint64_t invoiceNum, const std::string& poNum,
                                                   const MMath::mm_t& ratePerHour ) = 0;
   virtual ~Invoice() {}
};

struct InvoiceCustom : Invoice {
   std::optional<const std::string> write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                           uint64_t invoiceNum, const std::string& poNum,
                                           const MMath::mm_t& ratePerHour ) final;
   virtual ~InvoiceCustom() {}
};

struct InvoiceColor : Invoice {
   std::optional<const std::string> write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                           uint64_t invoiceNum, const std::string& poNum,
                                           const MMath::mm_t& ratePerHour ) final;

   virtual ~InvoiceColor() {}

  private:
   void writeProvider( std::ofstream& outfileTex, uint64_t invoiceNum );
   void writeCustomer( std::ofstream& outfileTex, const std::string& poNum );
   void writeFooter( std::ofstream& outfileTex );
};
