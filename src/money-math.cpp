#include "money-math.hpp"

#include <array>
#include <iostream>

#include "logging.hpp"

using std::abs;
using std::array;
using std::cout;
using std::optional;
using std::string;
using std::to_string;

namespace MMath {

string formatString( const int64_t ival, const int prec, const eNumFormat nf ) {
   (void)( nf );

   string rawStr = to_string( ival );

   if ( ival == 0L )
      return rawStr;

   array<char, 128> oStr;
   string           ooStr;
   if ( rawStr.size() > 10 )
      ooStr.reserve( 128u );

   /* Deal with the minus sign if present */
   size_t len = rawStr.size();
   size_t rawIdx{ 0 };
   size_t oIdx{ 0 };
   if ( ival < 0 ) {
      oStr.at( oIdx++ ) = '-';
      len--;
      rawIdx++;
   }

   /* Calculate the pos of the decimal and comma(s) */
   size_t firstCommaPos{ 0 };
   size_t intPartLen = len - static_cast<size_t>( prec );

   /* Calc the first comma pos since it depends on num size */
   if ( intPartLen > 3 ) {
      firstCommaPos = intPartLen % 3;
      if ( firstCommaPos == 0 )
         firstCommaPos = 3;

      /* Write the integer part with commas up to the last comma */
      size_t digitsToWrite = firstCommaPos;
      while ( intPartLen >= 3 ) {
         intPartLen -= digitsToWrite;

         while ( digitsToWrite-- ) {
            oStr.at( oIdx++ ) = rawStr[rawIdx++];
         }

         /* Need better comment here */
         if ( intPartLen ) {
            oStr.at( oIdx++ ) = ',';
            digitsToWrite     = 3;
         }
      }
   } else {
      /* Number has no commas so just write the digits */
      intPartLen = len - static_cast<size_t>( prec );
      while ( intPartLen-- ) {
         oStr.at( oIdx++ ) = rawStr[rawIdx++];
      }
   }

   /* Now write the fractional part if number has precision > 0 */
   if ( prec ) {
      oStr[oIdx++] = '.';
      for ( int i{ 0 }; i < prec; ++i ) {
         oStr.at( oIdx++ ) = rawStr[rawIdx++];
      }
   }
   oStr.at( oIdx ) = 0x00;
   return oStr.data();
}

int64_t initWithPrec( int64_t value, int64_t precFactor, int64_t desiredPrecFactor ) {
   if ( desiredPrecFactor == precFactor ) {
      // no conversion required
      return value;
   } else if ( desiredPrecFactor > precFactor ) {
      return value * ( desiredPrecFactor / precFactor );
   } else {
      // conversion
      if ( auto res = div_rounded( value, precFactor / desiredPrecFactor ) ) {
         return *res;
      } else {
         return 0;
      }
   }
}

bool parseUnpacked( const string &input, int &sign, int64_t &before, int64_t &after, int &decimalDigits ) {
   const char dec_point{ '.' };
   const bool thousands_grouping{ false };
   const char thousands_sep{ ',' };

   enum StateEnum { IN_SIGN, IN_BEFORE_FIRST_DIG, IN_BEFORE_DEC, IN_AFTER_DEC, IN_END } state = IN_SIGN;
   enum ErrorCodes { ERR_WRONG_CHAR = -1, ERR_NO_DIGITS = -2, ERR_WRONG_STATE = -3, ERR_STREAM_GET_ERROR = -4 };

   before = after = 0;
   sign           = 1;

   int error{ 0 };
   int digitsCount{ 0 };
   int afterDigitCount{ 0 };

   for ( const char c : input ) {
      if ( state == IN_END )
         break;

      switch ( state ) {
         case IN_SIGN:
            if ( c == '-' ) {
               sign  = -1;
               state = IN_BEFORE_FIRST_DIG;
            } else if ( c == '+' ) {
               state = IN_BEFORE_FIRST_DIG;
            } else if ( ( c >= '0' ) && ( c <= '9' ) ) {
               state  = IN_BEFORE_DEC;
               before = static_cast<int>( c - '0' );
               digitsCount++;
            } else if ( c == dec_point ) {
               state = IN_AFTER_DEC;
            } else if ( ( c != ' ' ) && ( c != '\t' ) ) {
               state = IN_END;
               error = ERR_WRONG_CHAR;
            }
            // else ignore char
            break;
         case IN_BEFORE_FIRST_DIG:
            if ( ( c >= '0' ) && ( c <= '9' ) ) {
               before = 10 * before + static_cast<int>( c - '0' );
               state  = IN_BEFORE_DEC;
               digitsCount++;
            } else if ( c == dec_point ) {
               state = IN_AFTER_DEC;
            } else {
               state = IN_END;
               error = ERR_WRONG_CHAR;
            }
            break;
         case IN_BEFORE_DEC:
            if ( ( c >= '0' ) && ( c <= '9' ) ) {
               before = 10 * before + static_cast<int>( c - '0' );
               digitsCount++;
            } else if ( c == dec_point ) {
               state = IN_AFTER_DEC;
            } else if ( thousands_grouping && c == thousands_sep ) {
               ;  // ignore the char
            } else {
               state = IN_END;
            }
            break;
         case IN_AFTER_DEC:
            if ( ( c >= '0' ) && ( c <= '9' ) ) {
               after = 10 * after + static_cast<int>( c - '0' );
               afterDigitCount++;
               if ( afterDigitCount >= max_decimal_points )
                  state = IN_END;
            } else {
               state = IN_END;
               if ( digitsCount == 0 ) {
                  error = ERR_NO_DIGITS;
               }
            }
            break;
         default:
            error = ERR_WRONG_STATE;
            state = IN_END;
            break;
      }  // switch state

   }  // get char

   decimalDigits = afterDigitCount;

   if ( error >= 0 ) {
      if ( sign < 0 ) {
         before = -before;
         after  = -after;
      }

   } else {
      before = after = 0;
   }

   return ( error >= 0 );
}  // function

int64_t multDiv( const int64_t value1, const int64_t value2, int64_t divisor ) {
   /* we don't check for division by zero, the caller should */
   const int64_t value1int{ value1 / divisor };
   int64_t       value1dec{ value1 % divisor };

   const int64_t value2int{ value2 / divisor };
   int64_t       value2dec{ value2 % divisor };

   int64_t result{ value1 * value2int + value1int * value2dec };

   if ( value1dec == 0 || value2dec == 0 ) {
      return result;
   }

   if ( !isMultOverflow( value1dec, value2dec ) ) {  // no overflow
      int64_t resDecPart{ value1dec * value2dec };

      if ( auto res = div_rounded( resDecPart, divisor ) ) {
         resDecPart = *res;
      } else {
         resDecPart = 0;
      }
      result += resDecPart;
      return result;
   }

   /* minimize value1 & divisor */
   {
      int64_t c{ gcd( value1dec, divisor ) };
      if ( c != 1 ) {
         value1dec /= c;
         divisor /= c;
      }

      /* minimize value2 & divisor */
      c = gcd( value2dec, divisor );
      if ( c != 1 ) {
         value2dec /= c;
         divisor /= c;
      }
   }

   /* no overflow */
   if ( !isMultOverflow( value1dec, value2dec ) ) {
      int64_t resDecPart{ value1dec * value2dec };
      if ( auto res = div_rounded( resDecPart, divisor ) ) {
         result += *res;
         return result;
      }
   }

   /* overflow can occur - use less precise version */
   result +=
       round( static_cast<double>( value1dec ) * static_cast<double>( value2dec ) / static_cast<double>( divisor ) );
   return result;
}

bool isMultOverflow( int64_t value1, int64_t value2 ) {
   if ( value1 == 0 || value2 == 0 ) {
      return false;
   }

   if ( ( value1 < 0 ) != ( value2 < 0 ) ) {  // different sign
      if ( value1 == DEC_MIN_INT64 ) {
         return value2 > 1;
      } else if ( value2 == DEC_MIN_INT64 ) {
         return value1 > 1;
      }
      if ( value1 < 0 ) {
         return isMultOverflow( -value1, value2 );
      }
      if ( value2 < 0 ) {
         return isMultOverflow( value1, -value2 );
      }
   } else if ( value1 < 0 && value2 < 0 ) {
      if ( value1 == DEC_MIN_INT64 ) {
         return value2 < -1;
      } else if ( value2 == DEC_MIN_INT64 ) {
         return value1 < -1;
      }
      return isMultOverflow( -value1, -value2 );
   }

   return ( value1 > DEC_MAX_INT64 / value2 );
}

int64_t gcd( int64_t a, int64_t b ) {
   int64_t c{ 0 };
   while ( a != 0 ) {
      c = a;
      a = b % a;
      b = c;
   }
   return b;
}

optional<int64_t> div_rounded( int64_t a, int64_t b ) {
   int64_t divisorCorr{ abs( b ) / 2 };
   if ( a >= 0 ) {
      if ( DEC_MAX_INT64 - a >= divisorCorr ) {
         return { ( a + divisorCorr ) / b };
      } else {
         const int64_t i{ a / b };
         const int64_t r{ a - i * b };
         if ( r < divisorCorr ) {
            return { i };
         }
      }
   } else {
      if ( -( DEC_MIN_INT64 - a ) >= divisorCorr ) {
         return { ( a - divisorCorr ) / b };
      } else {
         const int64_t i{ a / b };
         const int64_t r{ a - i * b };
         if ( r < divisorCorr ) {
            return { i };
         }
      }
   }
   return {};
}

/*
  Converts sting of chars to decimal
  Handles the following formats ('.' is selected from locale info):
  \code
  123
  -123
  123.0
  -123.0
  123.
  .123
  0.
  -.123
  \endcode
  Spaces and tabs on the front are ignored.
  Performs rounding when provided value has higher precision than in output type.
  \param[in] input input stream
  \param[out] output decimal value, 0 on error
  \result Returns true if conversion succeeded
 */
optional<int64_t> fromString( const string &input, int64_t decFactor, int prec ) {
   int     sign{ 0 };
   int     afterDigits{ 0 };
   int64_t before{ 0 };
   int64_t after{ 0 };
   int64_t result{ 0 };

   if ( parseUnpacked( input, sign, before, after, afterDigits ) ) {
       LOGF( eLogLevel::Info, "After parse_unpacked s[%d], before[%ld], after[%ld], afterdigits[%d]\n", sign, before, after,
            afterDigits );

      if ( afterDigits <= prec ) {
         /* direct mode */
         int corrCnt{ prec - afterDigits };
         while ( corrCnt > 0 ) {
            after *= 10;
            --corrCnt;
         }

         if ( prec > 0 ) {
            result = before * decFactor;
            result += ( after % decFactor );
         } else {
            result = before * decFactor;
         }
         LOGF( eLogLevel::Info, "Output pack before[%ld] after[%ld]\n", before, after );
         // output.pack( before, after );
      } else {
         /* rounding mode */
         int     corrCnt{ afterDigits };
         int64_t decimalFactor{ 1 };
         while ( corrCnt > 0 ) {
            before *= 10;
            decimalFactor *= 10;
            --corrCnt;
            LOGF( eLogLevel::Info, "Output pack temp [%ld] after[%ld] decimalFact[%ld]\n", before, after, decimalFactor );
         }

         /* decimal_type temp( before + after, decFactor ); */
         /* output = temp; */
         if ( auto res = div_rounded( before + after, decimalFactor / decFactor ) ) {
            result = *res;
         } else {
            return {};
         }
      }
      return { result };
   }
   return {};
}

}  // namespace MMath
