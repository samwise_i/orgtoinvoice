
#include "process-org.hpp"

#include <cassert>   // for assert
#include <fstream>   // for ifstream
#include <iostream>  // for operator<<, basic_ostream, basic_istream
#include <memory>    // for unique_ptr
#include <optional>  // for optional
#include <vector>    // for vector

#include "clock.hpp"        // for Clock_t, FilterDayMonYear, ProcessClock
#include "database.hpp"     // for TimeLog, eCommentType
#include "error-stuff.hpp"  // for putError, printError
#include "line-state.hpp"   // for eAction, StateEval, eAction::aError, eAct...
#include "logging.hpp"      // for LOGF
#include "utils.hpp"        // for RemoveLeadingChars, ClockLineLexer, Deter...

using org::err::printError;
using org::err::putError;
using std::cout;
using std::ifstream;
using std::ofstream;
using std::stof;
using std::string;
using std::to_string;
using std::unique_ptr;
using std::vector;

/*
  This function opens the specified file and only processes entries that fall within the
  specified day, month and year.
*/
int ProcessOrgFile( const std::filesystem::path &inFileName, int dBeg, int dEnd, int month, int year,
                    org::db::TimeLog *const pTL ) {
   ifstream infile;
   infile.open( inFileName );
   if ( !infile.is_open() ) {
      printError( "Error, unable to open .org input file[%s] ", inFileName.c_str() );
      return -1;
   }

   vector<Clock_t> clockList;
   string          tempS;

   using enum eAction;
   while ( getline( infile, tempS ) ) {
      tempS = org::utils::RemoveLeadingChars( tempS, " " );

      if ( (  tempS.empty() ) || (  tempS[0] == '\r') )
         continue;

      LOGF( eLogLevel::Info, "processing line [%s]\n", tempS.c_str() );

      org::utils::eLineTypeClk lt = org::utils::DetermineLineType( tempS );

      /* Determine action to take based on line type read from file */
      eAction action = StateEval( lt );

      // DBG int cnt{0};
      LOGF( eLogLevel::Info, "Line type[%d], action[%d]\n", lt, action );

      // using enum org::db::eCommentType;
      switch ( action ) {
         case aProcClk: {
            auto clex = org::utils::ClockLineLexer( tempS );

            if ( numberClockTokens != clex.size() ) {
               putError( "Invalid number of tokens found in clock line" );
               cout << "@ line : " << tempS << '\n';
               return -1;
            }

            if ( auto clock = ProcessClock( clex ) ) {
               if ( FilterDayMonYear( *clock, dBeg, dEnd, month, year ) ) {
                  clockList.push_back( *clock );
                  LOGF( eLogLevel::Info, "Added year[%d], month[%d], day[%d]\n", clock->year, clock->month,
                        clock->day );
               } else {
                  LOGF( eLogLevel::Info, "Skipped year[%d], month[%d], day[%d]\n", clock->year, clock->month,
                        clock->day );
               }
            } else {
               cout << "@ line : " << tempS << '\n';
               return -1;
            }
         } break;

         case aProcWrk:
            tempS = org::utils::RemoveLeadingChars( tempS, " ~" );
            if ( clockList.size() > 0 ) {
               if ( pTL->Update( org::db::eCommentType::eNormalWork, tempS, clockList ) ) {
                  clockList.clear();
               } else {
                  return -1;
               }
            }
            break;

         case aNone:
            break;

         case aProcMeet:
            if ( pTL->Update( org::db::eCommentType::eMeeting, tempS, clockList ) ) {
               clockList.clear();
            } else {
               return -1;
            }
            break;

         case aError:
            /* TODO: Not sure how we should handle this  */
            putError( "Error reading input file\n" );
            return -1;

         default:
            /* We should never get here */
            assert( false );

      } /* End Switch action */

   } /* End of read file loop */

   return 0;
}
