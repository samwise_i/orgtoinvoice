#pragma once

#include <filesystem>
#include <string>

int         ProcessConfigFile( const std::filesystem::path& inFileName );
std::string GetConfigData( const std::string& key );
