#include <cstdio>
#include <memory>
#include <optional>
#include <string>
#include <utility>

#include "LaTeX-helper.hpp"
#include "cfg-reader.hpp"
#include "database.hpp"
#include "error-stuff.hpp"
#include "money-math.hpp"
#include "utils.hpp"
#include "worklog.hpp"

using org::err::printError;
using std::cout;
using std::fixed;
using std::ofstream;
using std::optional;
using std::showpoint;
using std::string;
using std::string_view;

using MMath::mm_t;
using MMath::MoneyMath_cast;

/* clang-format off */
static constexpr string_view setup[] = {
   R"(\documentclass[10pt]{report})",
   R"(\usepackage[T1]{fontenc})",
   R"(\usepackage{lmodern})",
   R"(\usepackage{pdflscape})",
   R"(\usepackage{booktabs})",
   /* Better for color tables */
   R"(\usepackage{tabu})",
   /* Enable color */
   R"(\usepackage{color, colortbl})",
   R"(\usepackage[table]{xcolor})",
   /* Do not indent paragraphs */
   R"(\usepackage[parfill]{parskip})",
   /* Use most of the page */
   R"(\usepackage[hmargin=0.5in,vmargin=0.5in]{geometry})"
   /* Grab the user colors */
   R"(\usepackage{import})",
   R"(\import{./}{worklog-color.tex})"
};
/* clang-format on */

optional<const string> WorkLogColor::write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                            const string& payPeriod, bool verbose ) {
   string curDate = org::utils::GetCurDate( "%d %b %Y" );

   std::filesystem::path outfilename{ fileName };
   outfilename += ".log.tex";

   ofstream outfileTex( outfilename );
   if ( !outfileTex.is_open() ) {
      printError( "Error, failed to open output log Tex file[%s]\n", outfilename.c_str() );
      return {};
   }

   mm_t hours{ 0.0f };
   mm_t totalHours{ 0.0f };
   mm_t totalHoursWork{ 0.0f };
   mm_t hoursWork{ 0.0f };

   /* Setup the document */
   for ( auto& config : setup ) {
      outfileTex << config << '\n';
   }

   /* Start the document */
   outfileTex << R"(\begin{document})" << '\n';
   outfileTex << R"(\begin{landscape})" << '\n';
   outfileTex << R"(\fontfamily{lmss}\selectfont)" << '\n';

   outfileTex << R"(\def \tab {\hspace*{3ex}} % Define \tab to create some horizontal white space)" << '\n';

   /* Header */
   outfileTex << R"({\color{MyDarkBlue}\hrule height4mm})" << '\n';

   WrapTex wt( payPeriod, "MyDarkBlue" );
   outfileTex << R"({ \LARGE Work Log : )" << GetConfigData( "PROVIDER-company" ) << R"(\hfill \large )" << *wt
              << R"(} \\)" << '\n';

   outfileTex << R"(\vspace*{2ex} \\)" << '\n';

   outfileTex << R"(\taburulecolor{tablearraycolor})" << '\n';
   outfileTex << R"(\begin{tabu}{l@{\hspace{ 2em }}rl})" << '\n';

   outfileTex << R"(\rowfont{\color{tableheadercolor}})" << '\n';
   outfileTex << R"({\large Date} & {\large Work} & {\large Description} \\)" << '\n';
   outfileTex << R"(\toprule[1.5pt])" << '\n';

   char outStr[256];
   for ( const auto& [date, info] : db.GetTimeData() ) {
      hours = MoneyMath_cast<2>( info.minutesWork ) / MoneyMath_cast<2>( 60.00 );
      totalHours += hours;

      string comment  = org::utils::RemoveLeadingChars( info.commentWork, " `" );
      string eComment = org::utils::EscapifyComment( comment.data() );

      if ( true == verbose ) {
         snprintf( outStr, sizeof( outStr ), "%10s %8s %s", date.c_str(), hours.toCString(), eComment.c_str() );
         puts( outStr );
      }

      /* Change the color of table entries */
      outfileTex << R"(\rowfont{\color{tablecolor}})" << '\n';

      snprintf( outStr, sizeof( outStr ), R"(%s & %s & %s \\)", date.c_str(), hours.toCString(), eComment.c_str() );
      outfileTex << R"(\small)" << outStr << '\n';
   }

   outfileTex << R"(\midrule)" << '\n';
   outfileTex << R"(\noalign{\vskip 2mm})" << '\n';
   snprintf( outStr, sizeof( outStr ), R"(\multicolumn{1}{l}{\large Hours} & {\large %6s} \\)",
             totalHours.toCString() );
   outfileTex << outStr << '\n';

   /* Draw the bottom line of the table */
   outfileTex << R"(\noalign{\vskip 2mm})" << '\n';
   outfileTex << R"(\bottomrule[1.5pt])" << '\n';
   outfileTex << R"(\end{tabu})";

   /* Send total hours the the console  */
   if ( true == verbose ) {
      snprintf( outStr, sizeof( outStr ), "%10s %8s", "Total", totalHours.toCString() );
      puts( outStr );
   }

   // TODO: footer would go here

   /* End the document */
   outfileTex << R"(\end{landscape})";
   outfileTex << R"(\end{document})";

   return { totalHours.toString() };
}
