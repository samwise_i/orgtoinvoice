/*
  These functions are grouped together because C++ code with regex compiles extremely slow:-(
*/

#include "utils.hpp"

#include <stdio.h>     // for snprintf
#include <sys/stat.h>  // for time_t
#include <time.h>      // for localtime, strftime, time, tm

#include <algorithm>  // for for_each, max
#include <cctype>     // for isspace, tolower, toupper
#include <cstring>    // for strchr, size_t
#include <regex>      // for regex_search, operator==, match_results, regex

using std::for_each;
using std::optional;
using std::pair;
using std::regex;
using std::smatch;
using std::string;
using std::string_view;
using std::tolower;
using std::vector;

namespace org::utils {

using enum eLineTypeCFG;

// TODO : Create a function to check for these special chars in comment strings and escape as necessary
string EscapifyComment( const string &comment ) {
   constexpr char LaTeXspecial[]{ R"(&%$#_{}~^\)" };
   string         eString;
   for ( const char c : comment ) {
      if ( nullptr != strchr( LaTeXspecial, c ) ) {
         eString.push_back( '\\' );
      }
      eString.push_back( c );
   }
   return eString;
}

/*
  string formatedInvoice =std::format("{:06}", invoiceNum);
  This is a kludge until std::format is fixed
*/
string FormatInvoice( uint64_t invoiceNum ) {
   char formattedInv[24];
   snprintf( formattedInv, sizeof( formattedInv ), "%05lu", invoiceNum );
   string inv = formattedInv;
   return inv;
}

/*
  Grab a month from a org-mode date format
  CLOCK: [2021-09-07 Tue 10:32]--[2021-09-07 Tue 11:49] =>  1:17
*/
int GetMonth( string_view &dateValue ) {
   size_t monthBegin = dateValue.find_first_of( "-" );
   // Make sure we have a valid date
   if ( monthBegin == string::npos )
      return -1;
   string tempS{ dateValue.substr( monthBegin + 1, 2 ) };
   return stoi( tempS );
}

string GetCurDate( const char *fmtStr ) {
   time_t    now = time( nullptr );
   struct tm tim = *( localtime( &now ) );

   char curDate[30];
   strftime( curDate, sizeof( curDate ), fmtStr, &tim );
   return curDate;
}

string RemoveTrailingChar( const string &str, char ignoreChar ) {
   if ( str.length() < 2 )
      return str;

   auto pos = str.find_last_of( ignoreChar );

   /* None found so just return the string */
   if ( pos == string::npos )
      return str;

   while ( pos-- ) {
      if ( str[pos] != ignoreChar )
         break;
   }
   return str.substr( 0, pos + 1 );
}

string RemoveLeadingChars( const string &str, const char *ignoreChars ) {
   auto strBegin = str.find_first_not_of( ignoreChars );

   /* Make sure we found some */
   if ( strBegin == string::npos )
      return "";

   return str.substr( strBegin );
}

/*
  We currently support only two different line types:
    Section which is surrounded by brackets -> [PROVIDER]
    Key value pair seperated by '=' and value surrounded by quotes -> company = "Samuel Wise"
*/
pair<eLineTypeCFG, string> DetermineLineCFG( const string &line ) {
   regex  rSECTION{ R"rx(\[([a-zA-Z]+)])rx" };
   smatch match;
   if ( regex_search( line, match, rSECTION ) ) {
      return { eLineSection, match[1] };
   }

   int cnt{ 0 };
   for ( const char c : line ) {
      if ( c == '=' )
         cnt++;
   }
   if ( cnt == 1 ) {
      return { eLineNameVal, "" };
   }
   return { eLineUnknownCFG, "" };
}

/*
  We currently support only three different line types:
    CLOCK: [2017-02-14 Tue 13:16]--[2017-02-14 Tue 15:22] =>  2:06
    ~ Work line
    ` Meeting line
    ^ Travel line
*/
eLineTypeClk DetermineLineType( const string &line ) {
   smatch match;

   regex rCLOCK{ "^CLOCK:" };
   if ( regex_search( line, match, rCLOCK ) ) {
      return eLineClock;
   }

   regex rSECTION{ "^[\\^`~]" };
   if ( regex_search( line, match, rSECTION ) ) {
      if ( '~' == match[0] ) {
         return eLineWork;
      } else if ( match[0] == '`' ) {
         return eLineMeeting;
      } else if ( match[0] == '^' ) {
         return eLineTravel;
      }
   }
   return eLineUnKnown;
}

/*
  Take one of these appart and put it into a vector of strings:
  CLOCK: [2017-02-14 Tue 13:16]--[2017-02-14 Tue 15:22] =>  2:06
*/
vector<string> ClockLineLexer( const string &line ) {
   /* Remove the -- since we have to have spaces seperating all fields */
   auto   mre  = regex( "--" );
   string test = regex_replace( line, mre, "  " );

   string         token;
   vector<string> tokens;
   tokens.reserve( 32 );

   for_each( test.begin(), test.end(), [&]( char c ) {
      if ( !isspace( c ) ) {
         token += c;
      } else {
         if ( token.length() )
            tokens.push_back( token );
         token.clear();
      }
   } );
   if ( token.length() )
      tokens.push_back( token );

   return tokens;
}

/*
  Get key and value from our config file.
  company = "ACME Corp, Inc."
  Where '=' is the sep value
*/
pair<string, string> ParseKeyValue( const string &str, char sep ) {
   /* Grab the name */
   string tempS  = RemoveLeadingChars( str, " " );
   auto   sepPos = tempS.find_first_of( sep );
   string key    = RemoveTrailingChar( tempS.substr( 0, sepPos ), ' ' );

   /* Now extract the value using a regex on only using substring -> match[1] */
   string value = tempS.substr( sepPos + 1 );
   regex  rVALUE{ R"rx("(.*?)")rx" };
   smatch match;
   if ( regex_search( value, match, rVALUE ) ) {
      return { key, match[1].str() };
   }
   return { "", "" };
}

/*
  Make sure that our date is in the format yyyy-m(m)-d(d)
  ex. 2017-10-02 with some validation
*/
optional<string> GetData( const string &input ) {
   const regex rDATE{ R"rx(\d{4}-\d{1,2}-\d{1,2})rx" };

   smatch match;
   if ( regex_search( input, match, rDATE ) ) {
      return { match[0] };
   }
   return {};
}

/*
  Read hours entry
  8:23 ok, 10:12 ok, but not 1:1
*/
string GetHours( const string &input ) {
   const regex rHOURS{ R"rx(\d{1,2}(:\d{2}))rx" };
   smatch      match;

   if ( regex_search( input, match, rHOURS ) ) {
      return match[0];
   }
   return "";
}

}  // end namespace org::utils
