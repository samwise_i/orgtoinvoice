#pragma once

#include <cassert>
#include <cstring>
#include <string>

class WrapTex {
  public:
   explicit WrapTex( std::string_view& str ) { initBuf( str.data() ); }
   explicit WrapTex( std::string_view& str, const char* color ) {
      initBuf( str.data() );
      addColor( color );
   }

   explicit WrapTex( const std::string& str ) { initBuf( str.data() ); }
   explicit WrapTex( const std::string& str, const char* color ) {
      initBuf( str.data() );
      addColor( color );
   }

   explicit WrapTex( const char* str ) { initBuf( str ); }
   explicit WrapTex( const char* str, const char* color ) {
      initBuf( str );
      addColor( color );
   }

   inline void assign( std::string_view& str ) { initBuf( str.data() ); }
   inline void assign( std::string_view& str, const char* color ) {
      initBuf( str.data() );
      addColor( color );
   }

   inline void assign( const std::string& str ) { initBuf( str.data() ); }
   inline void assign( const std::string& str, const char* color ) {
      initBuf( str.data() );
      addColor( color );
   }

   inline void assign( const char* str ) { initBuf( str ); }
   inline void assign( const char* str, const char* color ) {
      initBuf( str );
      addColor( color );
   }

   const char* operator*() const { return ( buf + beg ); }

   inline const char* toCString() const { return ( buf + beg ); }

   inline void addColor( const char* color ) {
      static constexpr char attr[]{ "\\textcolor{%s}{" };
      char                  tbuf[64];
      int                   attrSz = snprintf( tbuf, sizeof( tbuf ), attr, color );
      assert( attrSz > 0 );
      addAttr( tbuf, attrSz );
   }

   inline void addColorBox( const char* color ) {
      static constexpr char attr[]{ "\\colorbox{%s}{" };
      char                  tbuf[64];
      int                   attrSz = snprintf( tbuf, sizeof( tbuf ), attr, color );
      assert( attrSz > 0 );
      addAttr( tbuf, attrSz );
   }

  private:
   static constexpr int bufSz{ 256 };
   static constexpr int endGap{ 20 };
   char                 buf[bufSz];
   int                  beg{ 0 };
   int                  end{ 0 };

   inline void initBuf( const char* str ) {
      memset( buf, 0x00, bufSz );
      int sz = static_cast<int>( strlen( str ) );
      assert( sz < bufSz );

      int ofs = bufSz - sz - endGap;
      assert( ofs > 0 );
      memcpy( ( buf + ofs ), str, static_cast<size_t>( sz ) );

      beg = ofs;
      end = beg + static_cast<int>( sz );
   }

   inline void addAttr( const char* attr, int attrSz ) {
      assert( ( beg - attrSz ) > 0 );
      beg -= attrSz;
      memcpy( ( buf + beg ), attr, static_cast<size_t>( attrSz ) );
      buf[end++] = '}';
   }
};
