#pragma once

#include "utils.hpp"

enum class eAction : int { aNone, aProcClk, aProcWrk, aProcMeet, aProcTrav, aError };

eAction StateEval( org::utils::eLineTypeClk lineType );
