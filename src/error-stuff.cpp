#include "error-stuff.hpp"

#include <cstdarg>
#include <cstdio>

namespace org::err {

void putError( const char* errorString ) {
   puts( HIRed );
   puts( errorString );
   puts( ColorOff );
}

void printError( const char* format, ... ) {
   va_list args;
   puts( HIRed );
   va_start( args, format );
   vprintf( format, args );
   va_end( args );
   puts( ColorOff );
}

} // end namespace org::err
