#pragma once

#include <optional>
#include <string>
#include <vector>

/* CLOCK: [2017-02-14 Tue 17:14]--[2017-02-14 Tue 18:14] =>  1:00 */
/* Number of tokens found in an org-mode clock line */
constexpr int numberClockTokens{ 9 };

enum eClockTokenPositions { eStartDate = 1, eDayOfWeek = 2, eEndDate = 4, eEndDay = 5, eHours = 8 };

struct Clock_t {
   std::string date;
   std::string dayOfWeek;
   int         day{ 0 };
   int         month{ 0 };
   int         year{ 0 };
   int         minutes{ 0 };
};

struct DateStruct_t {
   std::string date;
   int         day{ 0 };
   int         month{ 0 };
   int         year{ 0 };

   bool operator==( const DateStruct_t &rhs ) const {
      if ( day != rhs.day )
         return false;
      if ( month != rhs.month )
         return false;
      if ( year != rhs.year )
         return false;
      return true;
   }
};

std::optional<Clock_t>     ProcessClock( const std::vector<std::string> &tokens );
std::optional<Clock_t>     CombineClocks( const std::vector<Clock_t> &cl );
std::optional<std::string> GetData( const std::string &input );
bool                       FilterDayMonYear( const Clock_t &ct, int bday, int eday, int m, int y );
