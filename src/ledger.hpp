#pragma once

#include <filesystem>
#include <string>

bool WriteLedgerEntry( const std::filesystem::path& baseFileName, const std::string& clientname,
                       const std::string& invoiceNum, const std::string& amount );
