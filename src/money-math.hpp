/*
  MoneyMath was created with the intent of only dealing with money calculations and keeping a tiny footprint.
  It was inspired by decimal.h by Piotr Likus, https://github.com/vpiotr/decimal_for_cpp.

  It focuses on accurately calculating currency values using fixed point operations.
*/

#pragma once

#include <cstdint>
#include <limits>
#include <optional>
#include <string>

namespace MMath {

inline constexpr int64_t DEC_MAX_INT64{ std::numeric_limits<int64_t>::max() };
inline constexpr int64_t DEC_MIN_INT64{ std::numeric_limits<int64_t>::min() };
inline constexpr int     max_decimal_points{ 18 };

enum eNumFormat { Currency, Number };

template <int Prec>
struct DecimalFactor {
   static const int64_t value{ 10 * DecimalFactor<Prec - 1>::value };
   static const int     prec{ Prec };
};

template <>
struct DecimalFactor<0> {
   static const int64_t value{ 1 };
   static const int     prec{};
};

template <>
struct DecimalFactor<1> {
   static const int64_t value{ 10 };
   static const int     prec{ 1 };
};

template <class T>
static int64_t trunc( T value ) {
   return static_cast<int64_t>( value );
}

std::optional<int64_t> div_rounded( int64_t a, int64_t b );
std::optional<int64_t> fromString( const std::string &input, int64_t decFactor, int prec );

std::string formatString( int64_t ival, int prec, eNumFormat nf = Currency );
int64_t     initWithPrec( int64_t value, int64_t precFactor, int64_t desiredPrecFactor );
bool        parseUnpacked( const std::string &input, int &sign, int64_t &before, int64_t &after, int &decimalDigits );
int64_t     gcd( int64_t a, int64_t b );
bool        isMultOverflow( int64_t value1, int64_t value2 );
int64_t     multDiv( int64_t value1, int64_t value2, int64_t divisor );
bool        div_rounded( int64_t &output, int64_t a, int64_t b );

/* Only Standard rounding is supported  */
template <typename T>
inline int64_t round( T value ) {
   T val1{};

   if ( value < 0.0 ) {
      val1 = value - 0.5;
   } else {
      val1 = value + 0.5;
   }
   int64_t intPart{ static_cast<int64_t>( val1 ) };
   return intPart;
}

template <typename T>
int64_t bankers_round( T value ) {
   T val1{};
   T decimals{};

   if ( value >= 0.0 ) {
      decimals = value - floor( value );
      if ( decimals > 0.5 ) {
         val1 = ceil( value );
      } else if ( decimals < 0.5 ) {
         val1 = floor( value );
      } else {
         bool is_even = ( static_cast<int64_t>( value - decimals ) % 2 == 0 );
         if ( is_even ) {
            val1 = floor( value );
         } else {
            val1 = ceil( value );
         }
      }
   } else {
      decimals = abs( value + floor( abs( value ) ) );
      if ( decimals > 0.5 ) {
         val1 = floor( value );
      } else if ( decimals < 0.5 ) {
         val1 = ceil( value );
      } else {
         bool is_even = ( static_cast<int64_t>( value + decimals ) % 2 == 0 );
         if ( is_even ) {
            val1 = ceil( value );
         } else {
            val1 = floor( value );
         }
      }
   }
   return static_cast<int64_t>( val1 );
}

template <int Prec>
struct MoneyMath {
   /* Our underlying data type to represent currency */
   int64_t ival{};

  public:
   explicit MoneyMath() = default;
   explicit MoneyMath( int32_t val ) { ival = DecimalFactor<Prec>::value * val; }
   explicit MoneyMath( int64_t val ) { ival = DecimalFactor<Prec>::value * val; }

   explicit MoneyMath( uint32_t val ) { ival = DecimalFactor<Prec>::value * val; }
   explicit MoneyMath( uint64_t val ) { ival = DecimalFactor<Prec>::value * val; }

   explicit MoneyMath( float fnum ) { ival = convertToNative( static_cast<double>( fnum ) ); }
   explicit MoneyMath( double dnum ) { ival = convertToNative( dnum ); }

   MoneyMath( int64_t value, int64_t precFactor ) {
      ival = initWithPrec( value, precFactor, DecimalFactor<Prec>::value );
   }
   MoneyMath( const std::string &value ) {
      if ( auto res = fromString( value, DecimalFactor<Prec>::value, DecimalFactor<Prec>::prec ) )
         ival = *res;
   }

   int64_t getRaw() const { return ival; }
   int64_t getPrecFactor() const { return DecimalFactor<Prec>::value; }

   const std::string toString( eNumFormat nf = Currency ) const {
      return formatString( ival, DecimalFactor<Prec>::prec, nf );
   };

   const char *toCString( eNumFormat nf = Currency ) const {
      return formatString( ival, DecimalFactor<Prec>::prec, nf ).c_str();
   };

   /* Standard math comparison functions, this requires C++20  */

   auto operator<=>( const MoneyMath & ) const = default;

   /* Standard math functions  */

   MoneyMath &operator+=( const MoneyMath &rhs ) {
      ival += rhs.ival;
      return *this;
   }

   const MoneyMath operator+( const MoneyMath &rhs ) const {
      MoneyMath result = *this;
      result.ival += rhs.ival;
      return result;
   }

   MoneyMath &operator-=( const MoneyMath &rhs ) {
      ival -= rhs.ival;
      return *this;
   }

   const MoneyMath operator-( const MoneyMath &rhs ) const {
      MoneyMath result = *this;
      result.ival -= rhs.ival;
      return result;
   }

   template <typename T>
   MoneyMath &operator/=( const T &rhs ) {
      *this /= static_cast<MoneyMath>( rhs );
      return *this;
   }

   template <typename T>
   const MoneyMath operator/( const T &rhs ) const {
      return *this / static_cast<MoneyMath>( rhs );
   }

   const MoneyMath operator/( const MoneyMath &rhs ) const {
      MoneyMath result = *this;
      result.ival      = multDiv( result.ival, DecimalFactor<Prec>::value, rhs.ival );
      return result;
   }

   MoneyMath &operator/=( const MoneyMath &rhs ) {
      ival = multDiv( ival, DecimalFactor<Prec>::value, rhs.ival );
      return *this;
   }

   template <typename T>
   const MoneyMath operator*( const T &rhs ) const {
      return *this * static_cast<MoneyMath>( rhs );
   }

   template <typename T>
   const MoneyMath operator*=( const T &rhs ) const {
      *this *= static_cast<MoneyMath>( rhs );
      return *this;
   }

   const MoneyMath operator*( const MoneyMath &rhs ) const {
      MoneyMath result = *this;
      result.ival      = multDiv( result.ival, rhs.ival, DecimalFactor<Prec>::value );
      return result;
   }

   MoneyMath &operator*=( const MoneyMath &rhs ) {
      ival = multDiv( ival, rhs.ival, DecimalFactor<Prec>::value );
      return *this;
   }

  private:
   int64_t convertToNative( double value ) {
      int64_t intPart  = round( value );
      double  fracPart = value - static_cast<double>( intPart );
      return round( static_cast<double>( DecimalFactor<Prec>::value ) * fracPart ) +
             DecimalFactor<Prec>::value * intPart;
   }
};

template <int Prec, class T>
MoneyMath<Prec> MoneyMath_cast( const T &arg ) {
   return MoneyMath<Prec>( arg.getRaw(), arg.getPrecFactor() );
}

template <int Prec>
MoneyMath<Prec> MoneyMath_cast( double arg ) {
   MoneyMath<Prec> result( arg );
   return result;
}

template <int Prec>
MoneyMath<Prec> MoneyMath_cast( float arg ) {
   MoneyMath<Prec> result( arg );
   return result;
}

template <int Prec>
MoneyMath<Prec> MoneyMath_cast( int32_t arg ) {
   MoneyMath<Prec> result( arg );
   return result;
}

template <int Prec>
MoneyMath<Prec> MoneyMath_cast( int64_t arg ) {
   MoneyMath<Prec> result( arg );
   return result;
}

/* Common types  */
using mm_t  = MoneyMath<2>;
using mm4_t = MoneyMath<4>;

}  // namespace MMath
