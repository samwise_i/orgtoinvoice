#include "cfg-reader.hpp"

#include <fstream>
#include <unordered_map>
#include <unordered_set>

#include "error-stuff.hpp"
#include "logging.hpp"
#include "utils.hpp"

using std::ifstream;
using std::string;
using std::unordered_map;
using std::unordered_set;

using org::err::printError;

static const unordered_set<string>   validSections{ "[PROVIDER]", "[CLIENT]", "[INVOICE]", "[LOG]" };
static const unordered_set<string>   validKeys{ "company", "address1",    "city",  "state",  "zip",  "phone",
                                              "rate",    "orientation", "color", "footer", "terms" };
static unordered_map<string, string> configData;

string GetConfigData( const string& key ) {
   auto search = configData.find( key );
   if ( search != configData.end() ) {
      return search->second;
   }
   LOGF( eLogLevel::Info, "Config key[%s] NOT found\n", key.c_str() );
   return "";
}

int ProcessConfigFile( const std::filesystem::path& inFileName ) {
   ifstream infile;
   infile.open( inFileName );
   if ( !infile.is_open() ) {
      printError( "Error, unable to open configuration cfg file: %s\n", inFileName.c_str() );
      return -1;
   }

   string tempS;
   string currentSection;
   while ( getline( infile, tempS ) ) {
      if ( ( tempS.empty() ) || ( tempS[0] == '\r' ) )
         continue;

      auto [lt, lineVal] = org::utils::DetermineLineCFG( tempS );

      switch ( lt ) {
         case org::utils::eLineTypeCFG::eLineSection: {
            auto search = validSections.find( tempS );
            if ( search != validSections.end() ) {
               org::utils::MakeUpperCase( lineVal );
               currentSection = lineVal.append( "-" );
            } else {
               printError( "Config file invalid section: %s\n", tempS.c_str() );
               return -1;
            }
         } break;

         case org::utils::eLineTypeCFG::eLineNameVal: {
            auto [key, val] = org::utils::ParseKeyValue( tempS, '=' );
            org::utils::MakeLowerCase( key );
            auto search = validKeys.find( key );
            if ( search != validKeys.end() ) {
               key             = currentSection + key;
               configData[key] = val;
               LOGF( eLogLevel::Info, "key : %s, val : %s\n", key.c_str(), val.c_str() );
            } else {
               printError( "Config file invalid key[%s]\n", key.c_str() );
               return -1;
            }
         } break;

         default:
            LOGF( eLogLevel::Info, "Config line %s skipped\n", tempS.c_str() );
            break;
      }
   }
   return 0;
}
