#pragma once

#include <bits/chrono.h>  // for duration_cast, chrono, duration, microseconds
#include <stdio.h>        // for puts, printf, snprintf, vsnprintf

#include <cstdarg>  // for va_end, va_list, va_start

enum class eLogLevel : int { Error = 1, Warning = 2, Info = 3 };

#if defined( LOGGING )
#define LOG( level, msg ) logging::log( level, msg );
#define LOGF( level, format, ... ) logging::logf( level, format, __VA_ARGS__ );
#else
#define LOG( level, msg )
#define LOGF( level, format, ... )
#endif

namespace {
inline void logHeader( eLogLevel level ) {
    char outBuf[32];

    auto nowMicroSecs = duration_cast<std::chrono::microseconds>( std::chrono::steady_clock::now().time_since_epoch() ).count();
    snprintf( outBuf, sizeof( outBuf ), "[%d][%lu] - ", static_cast<int>( level ), nowMicroSecs );
    printf( "%s", outBuf );
}

}  // end anonymous namespace

namespace logging {

/* Log any events that are at this level or lower, higher numbers being more verbose */
inline constexpr int LOG_LEVEL{ 2 };

inline void log( eLogLevel level, const char* message ) {
    if ( static_cast<int>( level ) > LOG_LEVEL )
	return;
    logHeader( level );
    puts( message );
}

inline void logf( eLogLevel level, const char* format, ... ) {
    char outBuf[255];

    if ( static_cast<int>( level ) > LOG_LEVEL )
	return;

    logHeader( level );

    va_list args;
    va_start( args, format );
    vsnprintf( outBuf, sizeof( outBuf ), format, args );
    puts( outBuf );

    va_end( args );
}

}  // end namespace logging
