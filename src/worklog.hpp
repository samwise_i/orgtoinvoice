#pragma once

#include <filesystem>
#include <fstream>
#include <string>
#include <utility>

#include "database.hpp"
#include "error-stuff.hpp"

struct WorkLog {
   virtual std::optional<const std::string> write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                                   const std::string& payPeriod, bool verbose = false ) = 0;
   virtual ~WorkLog() {}
};

struct WorkLogCustom : WorkLog {
   std::optional<const std::string> write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                           const std::string& payPeriod, bool verbose = false ) final;

   virtual ~WorkLogCustom() {}
};

struct WorkLogColor : WorkLog {
   std::optional<const std::string> write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                           const std::string& payPeriod, bool verbose = false ) final;

   virtual ~WorkLogColor() {}
};

/* Your custom formats...  */
