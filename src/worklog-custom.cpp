#include <cstdio>
#include <memory>
#include <string>
#include <utility>

#include "LaTeX-helper.hpp"
#include "cfg-reader.hpp"
#include "database.hpp"
#include "error-stuff.hpp"
#include "money-math.hpp"
#include "worklog.hpp"
#include "utils.hpp"

using org::err::printError;
using std::cout;
using std::fixed;
using std::ofstream;
using std::showpoint;
using std::string;

using MMath::mm_t;
using MMath::MoneyMath_cast;

std::optional<const string> WorkLogCustom::write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                                  const string& payPeriod, bool verbose ) {
   /* Supress warning */
   (void)( payPeriod );

   string curDate = org::utils::GetCurDate( "%d %b %Y" );

   std::filesystem::path outfilename{ fileName };
   outfilename += ".log.tex";

   ofstream outfileTex( outfilename );
   if ( !outfileTex.is_open() ) {
      printError( "Error, failed to open output log Tex file[%s]\n", outfilename.c_str() );
      return {};
   }

   outfileTex.precision( 2 );
   outfileTex << showpoint;
   outfileTex << fixed;

   mm_t hours{ 0.0f };
   mm_t totalHours{ 0.0f };
   mm_t totalHoursWork{ 0.0f };
   mm_t hoursWork{ 0.0f };

   // TODO setup your custom LaTex formatting here

   char outStr[256];
   for ( const auto& [date, info] : db.GetTimeData() ) {
      hours = MoneyMath_cast<2>( info.minutesWork ) / MoneyMath_cast<2>( 60.00 );
      totalHours += hours;

      string comment  = org::utils::RemoveLeadingChars( info.commentWork, " `" );
      string eComment = org::utils::EscapifyComment( comment );

      snprintf( outStr, sizeof( outStr ), "%10s %8s %s", date.c_str(), hours.toCString(), eComment.c_str() );
      puts( outStr );

      snprintf( outStr, sizeof( outStr ), "%s & %s & %s", date.c_str(), hours.toCString(), eComment.c_str() );
   }

   if ( true == verbose ) {
      snprintf( outStr, sizeof( outStr ), "%10s %8s", "Total", totalHours.toCString() );
      puts( outStr );
   }
   return { totalHours.toString() };
}
