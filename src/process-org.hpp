#pragma once

#include <filesystem>
#include <string>

namespace org::db { class TimeLog; } 

int ProcessOrgFile( const std::filesystem::path &inFileName, int dBeg, int dEnd, int month, int year, org::db::TimeLog *const pTL );
