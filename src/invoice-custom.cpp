#include <cstdio>
#include <memory>
#include <string>
#include <utility>

#include "LaTeX-helper.hpp"
#include "cfg-reader.hpp"
#include "database.hpp"
#include "error-stuff.hpp"
#include "invoice.hpp"
#include "money-math.hpp"
#include "utils.hpp"

using org::err::printError;
using std::cout;
using std::fixed;
using std::ofstream;
using std::optional;
using std::showpoint;
using std::string;
using std::string_view;

using MMath::mm_t;
using MMath::MoneyMath_cast;

/*
  This is a non-working example template for creating a custom invoice in LaTeX format.
*/
optional<const string> InvoiceCustom::write( const std::filesystem::path& fileName, const org::db::TimeLog& db,
                                             uint64_t invoiceNum, const string& poNum, const mm_t& ratePerHour ) {
   string curDate = org::utils::GetCurDate( "%d %b %Y" );

   std::filesystem::path outfilename{ fileName };
   outfilename += ".tex";

   ofstream outfileTex( outfilename );
   if ( !outfileTex.is_open() ) {
      printError( "Error, failed to open output tex file[%s]\n", outfilename.c_str() );
      return {};
   }

   mm_t hours{ 0.0f };
   mm_t totalHours{ 0.0f };

   // TODO: Custom LaTeX setup

   outfileTex << R"({\LARGE )" << GetConfigData( "PROVIDER-company" ) << "} ";

   string formattedInv = org::utils::FormatInvoice( invoiceNum );
   outfileTex << R"({\large \hfill Invoice Number : )" << formattedInv << R"(} \\)" << '\n';

   outfileTex << GetConfigData( "PROVIDER-address1" ) << R"( \hfill )" << curDate << R"( \\)" << '\n';

   char outStr[256];
   snprintf( outStr, sizeof( outStr ), "%s, %s    %s", GetConfigData( "PROVIDER-city" ).c_str(),
             GetConfigData( "PROVIDER-state" ).c_str(), GetConfigData( "PROVIDER-zip" ).c_str() );

   outfileTex << outStr << R"(\\)" << '\n';

   outfileTex << R"(\vspace*{2ex} \\)" << '\n';
   outfileTex << R"({\LARGE Invoice To:})"
              << R"( \\)" << '\n';

   outfileTex << R"(\tab )" << GetConfigData( "CLIENT-company" );
   if ( poNum != "" ) {
      outfileTex << R"( \hfill PO : )" << poNum;
   }
   outfileTex << R"( \\)" << '\n';

   outfileTex << R"(\tab )" << GetConfigData( "CLIENT-address1" ) << R"( \\)" << '\n';
   snprintf( outStr, sizeof( outStr ), "%s, %s    %s", GetConfigData( "CLIENT-city" ).c_str(),
             GetConfigData( "CLIENT-state" ).c_str(), GetConfigData( "CLIENT-zip" ).c_str() );
   outfileTex << R"(\tab )" << outStr << '\n';

   mm_t   subtotal{ 0.0f };
   mm_t   totalAmount{ 0.0f };
   string rateStr = "\\$" + ratePerHour.toString() + R"({\tiny /hr})";
   string totalString{ "$" };

   /* Cycles through time data */
   for ( const auto& [date, info] : db.GetTimeData() ) {
      hours    = MoneyMath_cast<2>( info.minutesWork ) / MoneyMath_cast<2>( 60.00 );
      subtotal = hours * ratePerHour;
      totalHours += hours;
      totalAmount += subtotal;

      totalString += subtotal.toString();
      snprintf( outStr, sizeof( outStr ), R"(%s & %-6s & %-8s & \%-10s)", date.c_str(), hours.toCString(),
                rateStr.c_str(), totalString.c_str() );
      outfileTex << outStr << R"(\\)" << '\n';

      /* Only print one rate per invoice */
      rateStr     = "";
      totalString = "$";
   }

   snprintf( outStr, sizeof( outStr ), "~~Total  %10s    ~%10s\n", totalHours.toCString(), totalAmount.toCString() );
   totalString = "$";
   totalString += totalAmount.toString();
   snprintf( outStr, sizeof( outStr ), R"(\multicolumn{3}{r}{\large Total} & {\large \%s} \\)", totalString.c_str() );
   outfileTex << outStr << '\n';

   return { totalString };
}
