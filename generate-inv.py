import sys
import getopt
import os
import io
import collections
from os.path import exists
from datetime import date

PayPeriodTuple = collections.namedtuple( "period", ("month", "days", "per1", "per2") ) 

payPeriods = [
    PayPeriodTuple( "", 0,  [0, 0], [0, 0] ),
    PayPeriodTuple( "Jan", 30,  [1, 15], [16, 31] ),
    PayPeriodTuple( "Feb", 28,  [1, 14], [15, 28] ),
    PayPeriodTuple( "Mar", 31,  [1, 15], [16, 31] ),
    PayPeriodTuple( "Apr", 31,  [1, 15], [16, 30] ),
    PayPeriodTuple( "May", 31,  [1, 16], [15, 31] ),
    PayPeriodTuple( "Jun", 31,  [1, 15], [16, 30] ),
    PayPeriodTuple( "Jul", 31,  [1, 16], [17, 31] ),
    PayPeriodTuple( "Aug", 31,  [1, 15], [16, 31] ),    
    PayPeriodTuple( "Sep", 31,  [1, 15], [16, 30] ),
    PayPeriodTuple( "Oct", 31,  [1, 15], [16, 31] ),
    PayPeriodTuple( "Nov", 31,  [1, 15], [16, 30] ),
    PayPeriodTuple( "Dec", 31,  [1, 15], [16, 31] )
]

def sprint(*args, **kwargs):
    sio = io.StringIO()
    print(*args, **kwargs, file=sio)
    return sio.getvalue()

def sprintf(buf, fmt, *args):
    buf = io.StringIO()
    buf.write(fmt % args)

def help():
    for i in range(1,13):
        print( "%s Period 1 [%d thru %d], Period 2 [%d thru %d]" % (payPeriods[i].month, payPeriods[i].per1[0], payPeriods[i].per1[1], payPeriods[i].per2[0], payPeriods[i].per2[1]) )
    print('\n')
        
def exit(message):
	print(message + '\n')
	sys.exit()

def genConfigFileName(filename):
    fileNameAndPath =os.path.split(os.path.abspath(filename))
    nameAndExt = os.path.splitext(fileNameAndPath[1])
    configFileName = fileNameAndPath[0] + '/' + nameAndExt[0] + ".cfg"
    return configFileName

def getUserString(prompt):
    buf = io.StringIO()
    fmt ="%-45s : "
    buf.write(fmt % prompt)
    return input(buf.getvalue())

def getUserInt(prompt, min, max):
    buf = io.StringIO()
    fmt ="%-45s : "
    buf.write(fmt % prompt)
    val =int(input(buf.getvalue()))
    if (val < min) or (val > max ):
        return False, 0
    return True, val
    
def main(argv):

    print("\nOrgToInv Python3 Frontend ver 0.0.3, \xa9 Samwise 2021-22\n")

    filename = None

    opts, args = getopt.getopt(argv,"ht:",[])
    for opt, arg in opts:
        if(opt == "-h"):
            help()
            sys.exit()
        elif(opt == "-t"):
            filename = arg

    if( None == filename ):
        filename = getUserString("Enter the time data file(org)")

    if( exists(filename) == False ):
        exit("Org file not found.")
        
    # Check to see if we have a config file of the same name
    configFileName =genConfigFileName(filename)

    if( exists(configFileName) == False ):
        configFileName = getUserString("Enter the config file file(.cfg)")
        if( exists(filename) == False ):
            exit("Config file not found.")

    # Must check this here since we use it as an index
    status, month =getUserInt("Enter month[1-12]", 1, 12)
    if(False == status):
        exit("Invalid month.")

    # Deal with the pay period, not checked here since it will be checked by orgtoinv 
    pp =getUserString("Enter pay period[1-2] OR 0 for begin & end")
    if(pp == '0'):
        status, beg = getUserInt("Enter begin day [1,2..]", 1, 31)
        status, end = getUserInt("Enter end day [1,2..]", 1, 31)
        payPeriod = "-b %d -e %d" % (beg, end)
    else:
        payPeriod = "-p %s" % (pp)
        if( pp == '1'):
            beg = payPeriods[int(month)].per1[0]
            end = payPeriods[int(month)].per1[1]
        elif( pp =='2'):
            beg = payPeriods[int(month)].per2[0]
            end = payPeriods[int(month)].per2[1]

    year = getUserString("Enter year or <CR> to use current") 
    if(year == ""):
        todays_date = date.today()        
        year = todays_date.year
    else:
        year = int(year)
    
    invoiceNum = "%05d" % int(getUserString("Enter Invoice number"))

    # Create the file base file name
    baseNameAndPath =os.path.split(os.path.abspath(filename))
    nameAndExt = os.path.splitext(baseNameAndPath[1])
    baseFileName =baseNameAndPath[0] + '/' + nameAndExt[0] + '-' + str(invoiceNum) + '.' + payPeriods[int(month)].month + '-' + str(beg) + '-' + str(end) + '-' + str(year) 
    print(baseFileName)

    cmd ="which orgtoinv"
    if( os.system(cmd) ):
        exit("orgtoinv not found are you sure you installed it?")

    # Generate the LaTeX files
    cmd = sprint("orgtoinv -f", baseFileName, "-i", invoiceNum, payPeriod, "-m", month, "-y", year, "-t", os.path.abspath(filename), "-c", os.path.abspath(configFileName ) )
    print("Runing %s..." % cmd)
    if( os.system(cmd) ):
        exit("Failed to run orgtoinv.")

    cmd ="which pdflatex"
    if( os.system(cmd) ):
        exit("pdflatex not found, PDFs not produced.")

    # Convert to PDFs
    cmd ="pdflatex " + os.path.abspath(baseFileName) + ".tex"    
    print(cmd)
    if( os.system(cmd) ):
        exit("Failed to created invoice.")

    cmd ="pdflatex " + baseFileName + ".log.tex"    
    print(cmd)
    if( os.system(cmd) ):
        exit("Failed to created worklog.")


if __name__ == "__main__":
    main(sys.argv[1:])
        

