/*

  ASSERT_EQUAL(first, second)
    If first == second evaluates to false, the test will fail.
    first and second may be any type

  ASSERT_NOT_EQUAL(first, second)
    If first != second evaluates to false, the test will fail.
    first and second may be any type

  ASSERT_TRUE(bool value)
    If value is false, the test will fail.

  ASSERT_FALSE(bool value)
    If value is true, the test will fail.

  ASSERT_ALMOST_EQUAL(double first, double second, double precision)
    If first and second are not equal within precision, the test will fail.

  ASSERT_SEQUENCE_EQUAL(first, second)
    If first and second do not have equal elements, the test will fail.
    first and second may be any sequence types (e.g. arrays, vectors, lists),
    as long as their elements are comparable with ==.

*/

#include <unistd.h>

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

#include "cfg-reader.hpp"
#include "clock.hpp"
#include "database.hpp"
#include "error-stuff.hpp"
#include "invoice.hpp"
#include "line-state.hpp"
#include "logging.hpp"
#include "money-math.hpp"
#include "process-org.hpp"
#include "unit_test_framework.h"
#include "utils.hpp"
#include "worklog.hpp"

using namespace MMath;

using std::cout;
using std::ifstream;
using std::string;
using std::string_view;
using std::to_string;
using std::unique_ptr;
using std::vector;

using enum eLogLevel;
using enum eAction;

static org::db::TimeLog TL;
unique_ptr<Invoice>     pInvoice{ new InvoiceColor };
unique_ptr<WorkLog>     pWorkLog{ new WorkLogColor };

TEST( ClockProcessing ) {
   string tempS{ R"(CLOCK: [2021-12-07 Tue 08:13]--[2021-12-07 Tue 10:30] =>  2:17)" };
   auto   clex  = org::utils::ClockLineLexer( tempS );
   auto   clock = ProcessClock( clex );

   /* Workaround for test lib not liking std::optional */
   if ( clock ) {
      ASSERT_TRUE( clock->date == "2021-12-07" );
      ASSERT_EQUAL( clock->dayOfWeek, "Tue" );
      ASSERT_EQUAL( clock->day, 07 );
      ASSERT_EQUAL( clock->month, 12 );
      ASSERT_EQUAL( clock->year, 2021 );
      ASSERT_EQUAL( clock->minutes, 137 );
   } else {
      ASSERT_TRUE( false );
   }
}

TEST( LineType ) {
   using enum org::utils::eLineTypeCFG;
   using namespace org::utils;

   ASSERT_EQUAL( DetermineLineType( ":LOGBOOK:" ), eLineUnKnown );
   ASSERT_EQUAL( DetermineLineType( "CLOCK: [2017-02-14 Tue 13:16]--[2017-02-14 Tue 15:22] =>  2:06" ), eLineClock );
   ASSERT_EQUAL( DetermineLineType( "~ Worked with Elon on creating a release candidate and running the torture test" ),
                 eLineWork );
   ASSERT_EQUAL( DetermineLineType( "` Another meeting" ), eLineMeeting );
   ASSERT_EQUAL( DetermineLineType( "^ Flying!!!" ), eLineTravel );
}

TEST( ClockLineLexer ) {
   string tempS{ R"(CLOCK: [2021-12-07 Tue 08:13]--[2021-12-07 Tue 10:30] =>  2:17)" };
   auto   clex = org::utils::ClockLineLexer( tempS );

   ASSERT_EQUAL( clex.size(), 9 );
   ASSERT_EQUAL( clex[0], "CLOCK:" );
   ASSERT_EQUAL( clex[7], "=>" );
}

TEST( LineState ) {
   using enum eAction;
   using namespace org::utils;

   ASSERT_EQUAL( StateEval( eLineUnKnown ), aNone );
   ASSERT_EQUAL( StateEval( eLineWork ), aError );
   ASSERT_EQUAL( StateEval( eLineMeeting ), aError );
   ASSERT_EQUAL( StateEval( eLineTravel ), aError );

   ASSERT_EQUAL( StateEval( eLineClock ), aProcClk );
   ASSERT_EQUAL( StateEval( eLineUnKnown ), aNone );
   ASSERT_EQUAL( StateEval( eLineClock ), aProcClk );
   ASSERT_EQUAL( StateEval( eLineWork ), aProcWrk );

   ASSERT_EQUAL( StateEval( eLineClock ), aProcClk );
   ASSERT_EQUAL( StateEval( eLineUnKnown ), aNone );
   ASSERT_EQUAL( StateEval( eLineClock ), aProcClk );
   ASSERT_EQUAL( StateEval( eLineUnKnown ), aNone );
   ASSERT_EQUAL( StateEval( eLineMeeting ), aProcMeet );
   ASSERT_EQUAL( StateEval( eLineUnKnown ), aNone );

   ASSERT_EQUAL( StateEval( eLineClock ), aProcClk );
   ASSERT_EQUAL( StateEval( eLineUnKnown ), aNone );
   ASSERT_EQUAL( StateEval( eLineClock ), aProcClk );
   ASSERT_EQUAL( StateEval( eLineTravel ), aProcTrav );

   ASSERT_EQUAL( StateEval( eLineWork ), aError );
}

TEST( InvoiceGeneration ) {
   std::filesystem::path inputFile{ "../customer-test.org" };
   org::db::TimeLog      timeLog{};
   ASSERT_EQUAL( ProcessOrgFile( inputFile, 1, 14, 2, 2017, &timeLog ), 0 );

   MMath::mm_t ratehr{ 167.26 };
   auto        totalAmount = pInvoice->write( inputFile, timeLog, 1234ul, "1234", ratehr );

   string result = *totalAmount;
   ASSERT_EQUAL( result, "$5,549.69" );
}

TEST( WorkLogGeneration ) {
   std::filesystem::path inputFile{ "../customer-test.org" };
   org::db::TimeLog      timeLog{};
   ASSERT_EQUAL( ProcessOrgFile( inputFile, 1, 14, 2, 2017, &timeLog ), 0 );

   // using namespace MMath;
   MMath::mm_t ratehr{ 167.26 };
   auto        totalHours = pWorkLog->write( inputFile, timeLog, "PayPeriod" );

   string result = *totalHours;
   ASSERT_EQUAL( result, "33.18" );
}

TEST( MoneyMathArithmatic ) {
   MoneyMath<2> balance{ 0 };
   MoneyMath<2> a( 123 );
   MoneyMath<2> t( 246 );

   ASSERT_EQUAL( balance, mm_t( 0 ) );

   balance += a;
   ASSERT_NOT_EQUAL( balance, t );

   balance += a;
   ASSERT_EQUAL( balance, t );

   balance /= mm_t( 2 );
   ASSERT_EQUAL( balance, a );

   balance *= mm_t( 2 );
   ASSERT_EQUAL( balance, t );

   balance = balance / mm_t( 3 );
   ASSERT_EQUAL( balance, mm_t( 82 ) );

   balance = balance + mm_t( 3 );
   ASSERT_EQUAL( balance, mm_t( 85 ) );

   balance = balance - mm_t( 90 );
   ASSERT_EQUAL( balance, mm_t( -5 ) );

   balance = balance * mm_t( 9 );
   ASSERT_EQUAL( balance, mm_t( -45 ) );
   ASSERT_EQUAL( balance, mm_t( -45.0 ) );

   /* check div */
   balance = balance / mm_t( 10.0 );
   ASSERT_EQUAL( balance, mm_t( -4.5 ) );

   balance *= mm_t( 3.0 );
   ASSERT_EQUAL( balance, mm_t( -13.5 ) );

   /* assert( sizeof( mm_t::raw_data_t ) > 0 ); */
   balance /= mm_t( 10 );
   ASSERT_EQUAL( balance, mm_t( -1.35 ) );

   balance -= mm_t( 0.1 );
   ASSERT_EQUAL( balance, mm_t( -1.45 ) );

   balance = balance - mm_t( 3 );
   ASSERT_EQUAL( balance, mm_t( -4.45 ) );
}

TEST( MoneyMathMultiplyPrec ) {
   /* check mult prec */
   ASSERT_EQUAL( MoneyMath<4>( "0.0001" ) * MoneyMath<4>( "1.0000" ), MoneyMath<4>( "0.0001" ) );
   ASSERT_EQUAL( MoneyMath<4>( "0.0010" ) * MoneyMath<4>( "1.1000" ), MoneyMath<4>( "0.0011" ) );
   ASSERT_EQUAL( MoneyMath<4>( "2.0100" ) * MoneyMath<4>( "1.1000" ), MoneyMath<4>( "2.211" ) );
   ASSERT_EQUAL( MoneyMath<4>( "2.5010" ) * MoneyMath<4>( "1.5000" ), MoneyMath<4>( "3.7515" ) );

   /* check mult neg 1 */
   ASSERT_EQUAL( MoneyMath<4>( "-0.0001" ) * MoneyMath<4>( "1.0000" ), MoneyMath<4>( "-0.0001" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-0.0010" ) * MoneyMath<4>( "1.1000" ), MoneyMath<4>( "-0.0011" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-2.0100" ) * MoneyMath<4>( "1.1000" ), MoneyMath<4>( "-2.211" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-2.5010" ) * MoneyMath<4>( "1.5000" ), MoneyMath<4>( "-3.7515" ) );

   /* check mult neg 2 */
   ASSERT_EQUAL( MoneyMath<4>( "0.0001" ) * MoneyMath<4>( "-1.0000" ), MoneyMath<4>( "-0.0001" ) );
   ASSERT_EQUAL( MoneyMath<4>( "0.0010" ) * MoneyMath<4>( "-1.1000" ), MoneyMath<4>( "-0.0011" ) );
   ASSERT_EQUAL( MoneyMath<4>( "2.0100" ) * MoneyMath<4>( "-1.1000" ), MoneyMath<4>( "-2.211" ) );
   ASSERT_EQUAL( MoneyMath<4>( "2.5010" ) * MoneyMath<4>( "-1.5000" ), MoneyMath<4>( "-3.7515" ) );

   /* check mult both neg */
   ASSERT_EQUAL( MoneyMath<4>( "-0.0001" ) * MoneyMath<4>( "-1.0000" ), MoneyMath<4>( "0.0001" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-0.0010" ) * MoneyMath<4>( "-1.1000" ), MoneyMath<4>( "0.0011" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-2.0100" ) * MoneyMath<4>( "-1.1000" ), MoneyMath<4>( "2.211" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-2.5010" ) * MoneyMath<4>( "-1.5000" ), MoneyMath<4>( "3.7515" ) );

   /* check medium prec - near 32 bits */
   ASSERT_EQUAL( MoneyMath<9>( "1.5" ) * MoneyMath<9>( "2.1000" ), MoneyMath<9>( "3.15" ) );
   ASSERT_EQUAL( MoneyMath<9>( "1.000000005" ) * MoneyMath<9>( "2" ), MoneyMath<9>( "2.000000010" ) );
   ASSERT_EQUAL( MoneyMath<9>( "1.80000001" ) * MoneyMath<9>( "2" ), MoneyMath<9>( "3.600000020" ) );
   ASSERT_EQUAL( MoneyMath<9>( "1.80000001" ) * MoneyMath<9>( "2" ), MoneyMath<9>( "3.600000020" ) );

   ASSERT_EQUAL( MoneyMath<6>( "35000.000005" ) * MoneyMath<6>( "54123.654133" ), MoneyMath<6>( "1894327894.925618" ) );
}

TEST( MoneyMathMultiplyInt ) {
   ASSERT_EQUAL( MoneyMath<4>( "0.0001" ) * 2, MoneyMath<4>( "0.0002" ) );
   ASSERT_EQUAL( MoneyMath<4>( "0.1001" ) * 3, MoneyMath<4>( "0.3003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "1.1001" ) * 3, MoneyMath<4>( "3.3003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "1.0001" ) * 3, MoneyMath<4>( "3.0003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "1.0" ) * 3, MoneyMath<4>( "3.0" ) );

   ASSERT_EQUAL( MoneyMath<4>( "0.0001" ) * -2, MoneyMath<4>( "-0.0002" ) );
   ASSERT_EQUAL( MoneyMath<4>( "0.1001" ) * -3, MoneyMath<4>( "-0.3003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "1.1001" ) * -3, MoneyMath<4>( "-3.3003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "1.0001" ) * -3, MoneyMath<4>( "-3.0003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "1.0" ) * -3, MoneyMath<4>( "-3.0" ) );

   ASSERT_EQUAL( MoneyMath<4>( "-0.0001" ) * 2, MoneyMath<4>( "-0.0002" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-0.1001" ) * 3, MoneyMath<4>( "-0.3003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-1.1001" ) * 3, MoneyMath<4>( "-3.3003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-1.0001" ) * 3, MoneyMath<4>( "-3.0003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-1.0" ) * 3, MoneyMath<4>( "-3.0" ) );

   ASSERT_EQUAL( MoneyMath<4>( "-0.0001" ) * -2, MoneyMath<4>( "0.0002" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-0.1001" ) * -3, MoneyMath<4>( "0.3003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-1.1001" ) * -3, MoneyMath<4>( "3.3003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-1.0001" ) * -3, MoneyMath<4>( "3.0003" ) );
   ASSERT_EQUAL( MoneyMath<4>( "-1.0" ) * -3, MoneyMath<4>( "3.0" ) );
}

TEST( MoneyMathDivInt ) {
   ASSERT_EQUAL( MoneyMath<4>( "1.0001" ) / 2, MoneyMath<4>( "0.5001" ) );
   ASSERT_EQUAL( MoneyMath<4>( "2.0010" ) / 2L, MoneyMath<4>( "1.0005" ) );
}

TEST( MoneyMathRounding ) {
   MoneyMath<2> balance{ 0.0f };

   balance = mm_t( 3.156 );
   ASSERT_EQUAL( balance, mm_t( 3.16 ) );

   balance = mm_t( "3.155" );
   ASSERT_EQUAL( balance, mm_t( 3.16 ) );

   balance = mm_t( 3.154 );
   ASSERT_EQUAL( balance, mm_t( 3.15 ) );

   balance = mm_t( -3.156 );
   ASSERT_EQUAL( balance, mm_t( -3.16 ) );

   balance = mm_t( "-3.155" );
   ASSERT_EQUAL( balance, mm_t( -3.16 ) );

   balance = mm_t( -3.154 );
   ASSERT_EQUAL( balance, mm_t( -3.15 ) );

   balance = mm_t( 3.67 );
   balance = balance * mm_t( 3.67 );
   ASSERT_EQUAL( balance, mm_t( 13.47 ) );

   balance = mm_t( 3.67 );
   balance = balance / mm_t( 1.27 );
   ASSERT_EQUAL( balance, mm_t( 2.89 ) );

   /* test precision mixing */
   balance = mm_t( 3.67 );
   MoneyMath<6> crate( 1.4567 );
   balance = MoneyMath_cast<2>( ( MoneyMath_cast<6>( balance ) * crate ) );
   ASSERT_EQUAL( balance, mm_t( 5.35 ) );

   /* test int overflow */
   MoneyMath<10> largeValue( 1311.12176161 );
   balance = MoneyMath_cast<2>( largeValue * MoneyMath_cast<10>( 10 ) );
   ASSERT_EQUAL( balance, mm_t( 13111.22 ) );
}

TEST_MAIN()
